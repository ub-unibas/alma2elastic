if( init.Subfield === undefined ){
    "no subfield available"
} else {
    let result = {}
    if( init.Subfield['a'] !== undefined ){
        for (const [key, value] of Object.entries(init.Subfield['a'])) {
           result.Name = value
        }
    }
    if( init.Subfield['0'] !== undefined){
        for (const [key, value] of Object.entries(init.Subfield['0'])) {
            if( result.Identifier == undefined ){
                result.Identifier = Array()
            }
           result.Identifier.push(value)
        }
    }
    if( init.Subfield['4'] !== undefined ){
        for (const [key, value] of Object.entries(init.Subfield['4'])) {
            if( result.Type == undefined ){
                result.Type = Array()
            }
            result.Type.push(value)
        }
    }
    if( result.Identifier == undefined ){
        result.Identifier = Array("(noid)"+result.Name)
    }
    result
}