if( init.Subfield === undefined ){
    "no subfield available"
} else {
    let result = {
        "Name":"",
        "Identifier":"",
        "Type":""
    }
    if( init.Subfield['a'] !== undefined ){
        result.Name = init.Subfield['a']
    }
    if( init.Subfield['0'] !== undefined){
        result.Identifier = init.Subfield['0']
    }
    if( init.Subfield['4'] !== undefined ){
        result.Type = init.Subfield['4']
    }
    result
}