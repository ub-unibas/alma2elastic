package main

import (
	"bufio"
	"compress/gzip"
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"github.com/je4/elastic/v2/pkg/bulk"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/config"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/marc21"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"go.ub.unibas.ch/cloud/certloader/v2/pkg/loader"
	"io"

	"io/fs"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

const ConfigFileLocationEnvVariable = "CONFIG_FILE_LOCATION"

func main() {
	var err error

	currentTime := time.Now()
	currentTimeZone, offset := currentTime.Zone()
	fmt.Println("The Current time zone is:", currentTimeZone)
	fmt.Println("Time zone offset:", offset)
	fmt.Println("Current Time:", currentTime.String())
	fmt.Println("Current Time (UTC):", currentTime.UTC().String())

	var cfgFS fs.FS
	var cfgFile string
	if configFileLocation, ok := os.LookupEnv(ConfigFileLocationEnvVariable); ok {
		fmt.Println("Loading config file from environment: ", configFileLocation)
		cfgFS = os.DirFS(filepath.Dir(configFileLocation))
		cfgFile = filepath.Base(configFileLocation)
	} else {
		fmt.Println("Loading embedded config file")
		cfgFS = config.ConfigFS
		cfgFile = "dsv4ever01.toml"
	}
	conf := &Alma2ElasticConfig{ElasticIndex: "",
		ElasticIndexCreate: "",
		ElasticEndpoint:    []string{},
		ElasticV8:          true,
		ElasticAPIKey:      "",
		IndexerWorkers:     uint(runtime.NumCPU()),
	}
	if err := LoadAlma2ElasticConfig(cfgFS, cfgFile, conf); err != nil {
		log.Fatalf("cannot load toml from [%v] %s: %v", cfgFS, cfgFile, err)
	}

	fmt.Println("Initialize Logger")
	var loggerTLSConfig *tls.Config
	var loggerLoader io.Closer
	if conf.Log.Stash.TLS != nil {
		loggerTLSConfig, loggerLoader, err = loader.CreateClientLoader(conf.Log.Stash.TLS, nil)
		if err != nil {
			log.Fatalf("cannot create client loader: %v", err)
		}
		defer loggerLoader.Close()
	}

	logger, _logstash, _logfile, err := ublogger.CreateUbMultiLoggerTLS(conf.Log.Level, conf.Log.File,
		ublogger.SetDataset(conf.Log.Stash.Dataset),
		ublogger.SetLogStash(conf.Log.Stash.LogstashHost, conf.Log.Stash.LogstashPort, conf.Log.Stash.Namespace, conf.Log.Stash.LogstashTraceLevel),
		ublogger.SetTLS(conf.Log.Stash.TLS != nil),
		ublogger.SetTLSConfig(loggerTLSConfig),
	)
	if err != nil {
		log.Fatalf("cannot create logger: %v", err)
	}
	if _logstash != nil {
		defer _logstash.Close()
	}
	if _logfile != nil {
		defer _logfile.Close()
	}
	fmt.Println("Initialized logger successfully.")

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	var indexer bulk.Indexer
	indexer, err = bulk.NewIndexer8(conf.ElasticEndpoint, conf.ElasticIndex, string(conf.ElasticAPIKey), logger)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize elastic client")
	}
	logger.Info().Msgf("elastic client initialized")

	if conf.ElasticIndexCreate != "" {
		logger.Info().Msgf("loading schema '%s'", conf.ElasticIndexCreate)
		schemaBytes, schemaReadError := os.ReadFile(conf.ElasticIndexCreate)
		if schemaReadError != nil {
			logger.Fatal().Msgf("cannot read schema %s", conf.ElasticIndexCreate)
		}
		if err = indexer.CreateIndex(schemaBytes); err != nil {
			logger.Fatal().Msgf("Cannot delete index: %s", err)
		}
	}

	if err = indexer.StartBulk(int(conf.IndexerWorkers), int(5e+6), 30*time.Second); err != nil {
		logger.Fatal().Msgf("Error creating the indexer: %s", err)
	}
	defer indexer.CloseBulk()

	for _, file := range conf.Files {
		err = processFile(file, indexer, conf.IDField, logger)
		if err != nil {
			logger.Fatal().Msgf("Error processing file: %v", err)
		}
	}
}

func processFile(fileName string, indexer bulk.Indexer, idField string, logger *ublogger.Logger) error {
	logger.Info().Msgf("Processing file %s", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		err = file.Close()
		if err != nil {
			log.Fatalf("cannot close file: %v", err)
		}
	}(file)

	gzipReader, err := gzip.NewReader(file)
	if err != nil {
		return err
	}

	reader := bufio.NewReaderSize(gzipReader, 10*1024*1024)

	count := 0
	for {
		l, _, readLineError := reader.ReadLine()
		if readLineError != nil {
			logger.Error().Err(readLineError).Msgf("error reading line: %v", readLineError)
			break
		}
		line := string(l)
		count++
		if line == "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" || line == "</collection>" ||
			line == "<!-- fixed: 072 mit zwei $2, 245 mit leeren ind2 und ind1/ind2, 655 mit leerem ind2 -->" {
			logger.Debug().Msgf("Ignoring line: %s.", line)
			continue
		}
		var record = &marc21.Record{}
		sourceBuffer := strings.NewReader(line)
		xmlDecoder := xml.NewDecoder(sourceBuffer)
		if err = xmlDecoder.Decode(record); err != nil {
			log.Printf("xml decode fail on line %d: %s\n", count, line)
			return err
		}
		var esDocumentId string
		if idField != "" {
			fld035 := record.GetDatafields("035")
			if len(fld035) > 0 {
				for _, fld := range fld035 {
					for _, sub := range fld.Subfields {
						if sub.Code == "a" && strings.HasPrefix(sub.Text, idField) {
							esDocumentId = "id" + sub.Text[len(idField):]
							break
						}
					}
				}
			}
		}
		// adds field lists
		err = record.Optimize([]string{})
		if err != nil {
			return err
		}
		record.Timestamp = time.Now().Format("2006-01-02T15:04:05Z")
		if err = indexer.Index(esDocumentId, record); err != nil {
			logger.Error().Msgf("error indexing document '%s': %v", esDocumentId, err)
		}
	}
	return nil
}
