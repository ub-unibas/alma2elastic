package main

import (
	"emperror.dev/errors"
	"github.com/BurntSushi/toml"
	"github.com/je4/utils/v2/pkg/config"
	"github.com/je4/utils/v2/pkg/stashconfig"
	"io/fs"
	"os"
	"time"
)

type duration struct {
	time.Duration
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

type Alma2ElasticConfig struct {
	IDField            string             `toml:"idfield"`
	Files              []string           `toml:"files"`
	ElasticIndex       string             `toml:"elasticindex"`       // elastic search index to fill
	ElasticIndexCreate string             `toml:"elasticindexcreate"` //  deletes and creates elastic index with given schema
	ElasticEndpoint    []string           `toml:"elasticendpoint"`    // endpoint for elastic search
	ElasticV8          bool               `toml:"elasticv8"`          // use elastic search client version 8
	ElasticAPIKey      config.EnvString   `toml:"elasticapikey"`      // Apikey for elastic
	IndexerWorkers     uint               `toml:"indexerworkers"`     // number of concurrent bulk indexers
	Log                stashconfig.Config `toml:"log"`
}

func LoadAlma2ElasticConfig(fSys fs.FS, fp string, conf *Alma2ElasticConfig) error {
	if _, err := fs.Stat(fSys, fp); err != nil {
		path, err := os.Getwd()
		if err != nil {
			return errors.Wrap(err, "cannot get current working directory")
		}
		fSys = os.DirFS(path)
		fp = "alma2elastic.toml"
	}
	data, err := fs.ReadFile(fSys, fp)
	if err != nil {
		return errors.Wrapf(err, "cannot read file [%v] %s", fSys, fp)
	}
	_, err = toml.Decode(string(data), conf)
	if err != nil {
		return errors.Wrapf(err, "error loading config file %v", fp)
	}
	return nil
}
