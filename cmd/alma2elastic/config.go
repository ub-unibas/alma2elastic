package main

import (
	"emperror.dev/errors"
	"github.com/BurntSushi/toml"
	"github.com/je4/utils/v2/pkg/config"
	"github.com/je4/utils/v2/pkg/stashconfig"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type duration struct {
	time.Duration
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

type dtime struct {
	time.Time
}

func (dt *dtime) UnmarshalText(text []byte) error {
	var err error
	dt.Time, err = time.Parse("2006-01-02T15:04:05Z", string(text))
	return err
}

type Cron struct {
	Minute int
	Hours  []int
}

type EmbeddingConfig struct {
	Flags        []string         `toml:"flags"`
	CacheFolder  string           `toml:"cachefolder"`
	OpenaiApiUrl string           `toml:"openaiapiurl"`
	OpenaiApiKey config.EnvString `toml:"openaiapikey"`
	Template     string           `toml:"template"`
}

type Alma2ElasticConfig struct {
	IDField     string   `toml:"idfield"`
	Deduplicate bool     `toml:"deduplicate"`
	LocalFields []string `toml:"localfields"`
	LocalName   string   `toml:"localname"`
	GoogleOnly  bool     `toml:"googleonly"`

	GoogleAuth    config.EnvString `toml:"googleauth"`    // name of google api json authentication file
	ConfigTable   []string         `toml:"configtable"`   // google sheet id
	ConfigMapping string           `toml:"configmapping"` // field config sheet within table
	ConfigFacet   string           `toml:"configfacet"`   // facet config sheet within table
	ConfigFlag    string           `toml:"configflag"`    // flag config sheet within table
	ConfigJS      string           `toml:"configjs"`      // javascript config sheet within table
	ConfigACL     string           `toml:"configacl"`     // ACL config sheet within table
	ConfigService string           `toml:"configservice"` // service config sheet within table

	EndpointSet   string `toml:"endpoint_set"`   // OAI-PMH set
	Endpoint      string `toml:"endpoint"`       // OAI-PMH endpoint
	EndpointStart dtime  `toml:"endpoint_start"` // start date for harvester
	LastsyncFile  string `toml:"lastsyncfile"`   // file with last sync date

	LastSyncInGoogleSheet bool   `toml:"lastsync_in_google_sheet"`
	LastSyncName          string `toml:"lastsync_name"`
	LastSyncGoogleSheetID string `toml:"lastsync_google_sheet_id"`

	ElasticIndex       string           `toml:"elasticindex"`       // elastic search index to fill
	ElasticIndexCreate string           `toml:"elasticindexcreate"` //  deletes and creates elastic index with given schema
	ElasticEndpoint    []string         `toml:"elasticendpoint"`    // endpoint for elastic search
	ElasticV8          bool             `toml:"elasticv8"`          // use elastic search client version 8
	ElasticAPIKey      config.EnvString `toml:"elasticapikey"`      // Apikey for elastic

	JSDir              string `toml:"jsdir"`              // javascript directory
	JSVM               uint   `toml:"jsvm"`               // number of javascript isolations (VMs)
	JSRefreshIsolation uint64 `toml:"jsRefreshIsolation"` // number of uses before isolation will be discarded

	MapperWorkers   uint     `toml:"mapperworkers"`   // number of concurrent mappers
	MapperQueueSize uint     `toml:"mapperqueuesize"` // size of mapper queue
	MapperTimeout   duration `toml:"mappertimeout"`   // timeout for single mapping function
	IndexerWorkers  uint     `toml:"indexerworkers"`  // number of concurrent bulk indexers

	MaxDoc         uint `toml:"maxdocs"`        // stop after maxdocs
	EnableServices bool `toml:"enableservices"` // use rest services

	Cron                      Cron               `toml:"cron"`
	Embedding                 EmbeddingConfig    `toml:"embedding"`
	Log                       stashconfig.Config `toml:"log"`
	ServiceInsecureSkipVerify bool               `toml:"serviceinsecureskipverify"`
}

func LoadAlma2ElasticConfig(fSys fs.FS, fp string, conf *Alma2ElasticConfig) error {
	if _, err := fs.Stat(fSys, fp); err != nil {
		path, err := os.Getwd()
		if err != nil {
			return errors.Wrap(err, "cannot get current working directory")
		}
		fSys = os.DirFS(path)
		fp = "alma2elastic.toml"
	}
	data, err := fs.ReadFile(fSys, fp)
	if err != nil {
		return errors.Wrapf(err, "cannot read file [%v] %s", fSys, fp)
	}
	_, err = toml.Decode(string(data), conf)
	if err != nil {
		return errors.Wrapf(err, "error loading config file %v", fp)
	}
	if conf.JSDir != "" {
		conf.JSDir = strings.TrimRight(filepath.ToSlash(conf.JSDir), "/")
	}
	return nil
}
