package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"emperror.dev/errors"
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"github.com/dgraph-io/badger/v4"
	"github.com/dustin/go-humanize"
	"github.com/elastic/elastic-transport-go/v8/elastictransport"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/typedapi/types"
	"github.com/golang-queue/queue"
	"github.com/je4/elastic/v2/pkg/bulk"
	config2 "github.com/je4/utils/v2/pkg/config"
	"github.com/miku/metha"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/certs"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/config"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/embedding"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/gconfig"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/js"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/marc21"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/oaipmh"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"go.ub.unibas.ch/cloud/certloader/v2/pkg/loader"
	"golang.org/x/exp/slices"
	"golang.org/x/net/http2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"sync/atomic"
	"syscall"
	"text/template"
	"time"
)

//const IndexName = "je_iz_v1_20220909_00"

const SYNCTIMEFORMAT = time.RFC3339
const ConfigFileLocationEnvVariable = "CONFIG_FILE_LOCATION"

var configfile = flag.String("config", "", "location of toml configuration file")
var _endpoint = flag.String("endpoint", "", "url of oai pmh endpoint")
var _lastsyncfile = flag.String("lastsyncfile", "", "file with start date for oai-pmh harvester")
var _googleauth = flag.String("googleauth", "", "name of google api json authentication file")
var _configtable = flag.String("configtable", "", "google sheet id")
var _configfield = flag.String("configfield", "", "field config sheet within table")
var _configfacet = flag.String("configfacet", "", "facet config sheet within table")
var _configflag = flag.String("configflag", "", "flag config sheet within table")
var _configservice = flag.String("configservice", "", "service config sheet within table")
var _configacl = flag.String("configacl", "", "ACL config sheet within table")
var _index = flag.String("index", "", "elastic search index to fill")
var _indexcreate = flag.String("create", "", "deletes and creates elastic index with given schema")
var _elasticendpoint = flag.String("elastic", "", "endpoint for elastic search")
var _elasticV8 = flag.Bool("v8", false, "set, if elasticsearch version 8 client is needed")

// var _elasticAPIKey = flag.String("elasticapikey", "", "apikey for elasticsearch version 8 client")
var _mapperWorkers = flag.Uint("mappers", 0, "number of concurrent mapping workers")
var _mapperQueueSize = flag.Uint("mapperqueuesize", 0, "size of mapper queue")
var _indexerWorkers = flag.Uint("indexers", 0, "number of concurrent elastic indexing bulk workers")
var _maxDocs = flag.Uint("maxdocs", 0, "maximum number of documents to ingest")
var _jsDir = flag.String("jsfolder", "", "folder with javascript mapper functions")
var _jsVM = flag.Uint("jsvm", 0, "number of vm's for javascript interpreter")
var _enableServices = flag.Bool("enableservices", false, "use rest services while mapping")
var _single = flag.String("single", "", "single oai id to process")

type LocalField struct {
	Tag          string
	SubFieldCode string
	SubFieldText string
}

type LocalFields []*LocalField

func (lf LocalFields) Contains(fld *marc21.Datafield) bool {
	for _, l := range lf {
		if strings.HasPrefix(fld.Tag, l.Tag) {
			if l.SubFieldCode == "" {
				return true
			}
			for _, sf := range fld.Subfields {
				if sf.Code == l.SubFieldCode {
					if l.SubFieldText == "" {
						return true
					}
					return strings.ToLower(sf.Text) == l.SubFieldText
				}
			}
		}
	}
	return false
}

type MgetDoc struct {
	Index      string              `json:"_index"`
	ID         string              `json:"_id"`
	Found      bool                `json:"_found"`
	OAIId      []string            `json:"oai_id"`
	Leader     marc21.Leader       `json:"leader"`
	Datafields []*marc21.Datafield `json:"datafield"`
}

func setLocalFields(fields LocalFields, localname string, base []*marc21.Datafield) []*marc21.Datafield {
	base2 := []*marc21.Datafield{}
	for _, baseField := range base {
		if fields.Contains(baseField) {
			baseField.Subfields = append(baseField.Subfields, &marc21.Subfield{Code: "9", Text: localname})
		}
		base2 = append(base2, baseField)
	}
	return base2
}

func removeLocalFields(fields LocalFields, localname string, base []*marc21.Datafield) []*marc21.Datafield {
	base2 := []*marc21.Datafield{}
	for _, baseField := range base {
		if !fields.Contains(baseField) {
			base2 = append(base2, baseField)
			continue
		}
		var found bool = false
		for _, subfield := range baseField.Subfields {
			if subfield.Code == "9" && subfield.Text == localname {
				found = true
				break
			}
		}
		if !found {
			base2 = append(base2, baseField)
		}
	}
	return base2
}
func combineDatafields(fields LocalFields, localname string, existing, new []*marc21.Datafield) []*marc21.Datafield {
	// first remove all fields from existing
	existing = removeLocalFields(fields, localname, existing)

	// remove all existing local fields of current localname
	for _, existingLocalField := range existing {
		if fields.Contains(existingLocalField) {
			var isCurrent = false
			for _, subfield := range existingLocalField.Subfields {
				if subfield.Code == "9" && subfield.Text == localname {
					isCurrent = true
					break
				}
			} // for _, subfield := range existingLocalField.Subfields
			if !isCurrent {
				new = append(new, existingLocalField)
			}
		} // if fields.Contains(existingLocalField)
	} // for _, existingLocalField := range existing
	return new
}

type OAI_Datafield struct {
	OAIId  []string `json:"oai_id"`
	Leader struct {
		LeaderFull string `json:"leader_full"`
	} `json:"LDR"`
	Datafields []*marc21.Datafield `json:"datafield"`
}

func (od *OAI_Datafield) FromData(data []byte) error {
	return errors.Wrap(json.Unmarshal(data, od), "cannot unmarshal data")
}

func (od *OAI_Datafield) FromAny(data any) error {
	source, ok := data.(map[string]interface{})
	if !ok {
		return errors.Errorf("data not a map - %v", data)
	}
	od.OAIId = []string{}
	od.Datafields = []*marc21.Datafield{}
	od.Leader = struct {
		LeaderFull string `json:"leader_full"`
	}{}

	if leaderInt, ok := source["leader"]; ok {
		if leaderMap, ok := leaderInt.(map[string]interface{}); ok {
			if leaderFullInt, ok := leaderMap["leader_full"]; ok {
				od.Leader.LeaderFull, _ = leaderFullInt.(string)
			}
		}
	}
	if oaiIdInt, ok := source["oai_id"]; ok {
		oaiId, _ := oaiIdInt.([]interface{})
		for _, oi := range oaiId {
			oiStr, _ := oi.(string)
			if oiStr != "" {
				od.OAIId = append(od.OAIId, oiStr)
			}
		}
	}
	if datafieldsInt, ok := source["datafield"]; ok {
		datafields, _ := datafieldsInt.([]interface{})
		for _, datafieldInt := range datafields {
			datafield, _ := datafieldInt.(map[string]interface{})
			df := &marc21.Datafield{}
			if tagInt, ok := datafield["tag"]; ok {
				df.Tag, _ = tagInt.(string)
			}
			if ind1Int, ok := datafield["ind1"]; ok {
				df.Ind1, _ = ind1Int.(string)
			}
			if ind2Int, ok := datafield["ind2"]; ok {
				df.Ind2, _ = ind2Int.(string)
			}
			if subfieldsInt, ok := datafield["subfield"]; ok {
				subfields, _ := subfieldsInt.([]interface{})
				for _, subfieldInt := range subfields {
					subfield, _ := subfieldInt.(string)
					sf := &marc21.Subfield{}
					sf.Code = subfield[1:2]
					sf.Text = subfield[3:]
					df.Subfields = append(df.Subfields, sf)
				}
			}
			od.Datafields = append(od.Datafields, df)

		}
	}
	return nil
}

func elasticMgetDoc2Struct(doc any) (*MgetDoc, error) {
	var ret = &MgetDoc{}
	docMap, ok := doc.(map[string]interface{})
	if !ok {
		return nil, errors.Errorf("doc not a map - %v", doc)
	}
	if indexInt, ok := docMap["_index"]; ok {
		ret.Index, _ = indexInt.(string)
	}

	if idInt, ok := docMap["_id"]; ok {
		ret.ID, _ = idInt.(string)
	}
	if foundInt, ok := docMap["found"]; ok {
		ret.Found, _ = foundInt.(bool)
	}
	if ret.Found {
		if sourceInt, ok := docMap["_source"]; ok {

			oai_Datafield := &OAI_Datafield{}
			if err := oai_Datafield.FromAny(sourceInt); err != nil {
				return nil, errors.Wrap(err, "cannot convert source to oai_datafield")
			}
			ret.OAIId = oai_Datafield.OAIId
			ret.Datafields = oai_Datafield.Datafields
		}
	}
	return ret, nil
}
func elasticGetDoc2Struct(docMap map[string]any) (*MgetDoc, error) {
	var ret = &MgetDoc{}
	oai_Datafield := &OAI_Datafield{}
	if err := oai_Datafield.FromAny(docMap); err != nil {
		return nil, errors.Wrap(err, "cannot convert source to oai_datafield")
	}
	ret.OAIId = oai_Datafield.OAIId
	ret.Datafields = oai_Datafield.Datafields
	return ret, nil
}

func main() {
	var err error

	flag.Parse()

	current_time := time.Now()
	currentTimeZone, offset := current_time.Zone()
	fmt.Println("The Current time zone is:", currentTimeZone)
	fmt.Println("Time zone offset:", offset)
	fmt.Println("Current Time:", current_time.String())
	fmt.Println("Current Time (UTC):", current_time.UTC().String())

	var cfgFS fs.FS
	var cfgFile string
	if configFileLocation, ok := os.LookupEnv(ConfigFileLocationEnvVariable); ok {
		fmt.Println("Loading config file from environment: ", configFileLocation)
		cfgFS = os.DirFS(filepath.Dir(configFileLocation))
		cfgFile = filepath.Base(configFileLocation)
	} else if *configfile != "" {
		fmt.Println("Loading config file with cmd param: ", *configfile)
		cfgFS = os.DirFS(filepath.Dir(*configfile))
		cfgFile = filepath.Base(*configfile)
	} else {
		fmt.Println("Loading embedded config file")
		cfgFS = config.ConfigFS
		cfgFile = "alma2elastic.toml"
	}
	conf := &Alma2ElasticConfig{
		GoogleAuth:         "",
		ConfigTable:        []string{},
		ConfigMapping:      "Mapping",
		ConfigFacet:        "Facets",
		ConfigFlag:         "Flags",
		ConfigJS:           "Javascript",
		ConfigService:      "REST",
		ConfigACL:          "ACL",
		EnableServices:     false,
		Endpoint:           "",
		EndpointStart:      dtime{Time: time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)},
		ElasticIndex:       "",
		ElasticIndexCreate: "",
		ElasticEndpoint:    []string{},
		ElasticV8:          false,
		ElasticAPIKey:      "",
		JSDir:              "",
		LastsyncFile:       "",
		JSVM:               uint(runtime.NumCPU()),
		MapperWorkers:      uint(runtime.NumCPU()),
		MapperTimeout:      duration{Duration: 20 * time.Second},
		JSRefreshIsolation: 5000,
		MapperQueueSize:    2000,
		IndexerWorkers:     uint(runtime.NumCPU()),
		MaxDoc:             0,
	}
	if err := LoadAlma2ElasticConfig(cfgFS, cfgFile, conf); err != nil {
		log.Fatalf("cannot load toml from [%v] %s: %v", cfgFS, cfgFile, err)
	}
	var loggerTLSConfig *tls.Config
	var loggerLoader io.Closer
	if conf.Log.Stash.TLS != nil {
		loggerTLSConfig, loggerLoader, err = loader.CreateClientLoader(conf.Log.Stash.TLS, nil)
		if err != nil {
			log.Fatalf("cannot create client loader: %v", err)
		}
		defer loggerLoader.Close()
	}

	logger, _logstash, _logfile, err := ublogger.CreateUbMultiLoggerTLS(conf.Log.Level, conf.Log.File,
		ublogger.SetDataset(conf.Log.Stash.Dataset),
		ublogger.SetLogStash(conf.Log.Stash.LogstashHost, conf.Log.Stash.LogstashPort, conf.Log.Stash.Namespace, conf.Log.Stash.LogstashTraceLevel),
		ublogger.SetTLS(conf.Log.Stash.TLS != nil),
		ublogger.SetTLSConfig(loggerTLSConfig),
	)
	if err != nil {
		log.Fatalf("cannot create logger: %v", err)
	}
	if _logstash != nil {
		defer _logstash.Close()
	}
	if _logfile != nil {
		defer _logfile.Close()
	}

	logger.Info().Msgf("Starting alma2elastic with config %s", cfgFile)

	if *_endpoint != "" {
		conf.Endpoint = *_endpoint
	}
	if *_lastsyncfile != "" {
		conf.LastsyncFile = *_lastsyncfile
	}
	if *_googleauth != "" {
		conf.GoogleAuth = config2.EnvString(*_googleauth)
	}
	if *_configtable != "" {
		conf.ConfigTable = []string{*_configtable}
	}
	if *_configfield != "" {
		conf.ConfigMapping = *_configfield
	}
	if *_configfacet != "" {
		conf.ConfigFacet = *_configfacet
	}
	if *_configflag != "" {
		conf.ConfigFlag = *_configflag
	}
	if *_configservice != "" {
		conf.ConfigService = *_configservice
	}
	if *_configacl != "" {
		conf.ConfigACL = *_configacl
	}
	if *_index != "" {
		conf.ElasticIndex = *_index
	}
	if *_indexcreate != "" {
		conf.ElasticIndexCreate = *_indexcreate
	}
	if *_elasticendpoint != "" {
		conf.ElasticEndpoint = []string{*_elasticendpoint}
	}
	if *_mapperWorkers > 0 {
		conf.MapperWorkers = *_mapperWorkers
	}
	if *_mapperQueueSize > 0 {
		conf.MapperQueueSize = *_mapperQueueSize
	}
	if *_indexerWorkers > 0 {
		conf.IndexerWorkers = *_indexerWorkers
	}
	if *_maxDocs > 0 {
		conf.MaxDoc = *_maxDocs
	}
	if *_elasticV8 {
		conf.ElasticV8 = true
	}
	/*
		if *_elasticAPIKey != "" {
			conf.ElasticAPIKey = *_elasticAPIKey
		}
	*/
	if *_jsDir != "" {
		conf.JSDir = *_jsDir
	}
	if *_jsVM > 0 {
		conf.JSVM = *_jsVM
	}

	if *_enableServices {
		conf.EnableServices = true
	}

	if len(conf.ConfigTable) == 0 {
		logger.Fatal().Msg("no configtable parameter given")
	}

	if conf.ElasticIndex == "" {
		logger.Fatal().Msg("no index parameter")
	}

	if conf.IDField == "" && conf.Deduplicate {
		logger.Fatal().Msg("for deduplication an idfield must be given")
	}

	var gjson []byte
	gjson, err = os.ReadFile(string(conf.GoogleAuth))
	if err != nil {
		gjson, err = fs.ReadFile(config.ConfigFS, string(conf.GoogleAuth))
		if err != nil {
			logger.Fatal().Msgf("cannot read google authentication file %s: %v", conf.GoogleAuth, err)
		}
	}

	logger.Info().Msgf("google auth: %s", conf.GoogleAuth)

	gconf, err := google.JWTConfigFromJSON(gjson, sheets.SpreadsheetsScope)
	if err != nil {
		logger.Fatal().Msg("cannot authenticate google api")
	}

	jse, err := js.NewJSEngine(int(conf.JSVM), conf.JSRefreshIsolation, logger)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize v8 engine: %v", err)
	}
	defer jse.Close()

	logger.Info().Msgf("javascript engine loaded")

	gSheet, err := gconfig.NewGSheet(gconf)
	if err != nil {
		logger.Fatal().Msgf("cannot connect to google sheets: %v", err)
	}

	if conf.JSDir != "" {
		fsFS := os.DirFS(conf.JSDir)
		entries, err := fs.ReadDir(fsFS, ".")
		if err != nil {
			logger.Fatal().Msgf("read folder %s: %v", conf.JSDir, err)
		}
		for _, de := range entries {
			if !(strings.HasSuffix(de.Name(), ".js") || strings.HasSuffix(de.Name(), ".ts")) || de.IsDir() {
				continue
			}
			source, err := fs.ReadFile(fsFS, de.Name())
			if err != nil {
				logger.Fatal().Msgf("cannot read %s/%s: %v", conf.JSDir, de.Name(), err)
			}
			if err := jse.AddScript(de.Name(), string(source)); err != nil {
				logger.Fatal().Msgf("cannot add %s/%s to js cache: %v", conf.JSDir, de.Name(), err)
			}
		}
	}

	logger.Info().Msgf("javascript scripts loaded")

	var re *marc21.RESTEngine
	if conf.EnableServices {
		caCert, err := fs.ReadFile(certs.CertFS, "ca.cert.pem")
		if err != nil {
			logger.Fatal().Msgf("cannot read internal ca.cert.pem")
		}

		caCertPool, err := x509.SystemCertPool()
		if err != nil {
			caCertPool = x509.NewCertPool()
		}
		if caCert != nil {
			caCertPool.AppendCertsFromPEM(caCert)
		}

		// Create TLS configuration with the certificate of the server
		tlsConfig := &tls.Config{
			RootCAs:            caCertPool,
			InsecureSkipVerify: conf.ServiceInsecureSkipVerify,
		}
		//	httpClient := httputil.NewLimitedClient(300, time.Second*2, time.Second*10, caCert)

		httpClient := &http.Client{Transport: &http2.Transport{
			DialTLSContext:             nil,
			TLSClientConfig:            tlsConfig,
			ConnPool:                   nil,
			DisableCompression:         false,
			AllowHTTP:                  false,
			MaxHeaderListSize:          0,
			MaxReadFrameSize:           0,
			MaxDecoderHeaderTableSize:  0,
			MaxEncoderHeaderTableSize:  0,
			StrictMaxConcurrentStreams: false,
			ReadIdleTimeout:            0,
			PingTimeout:                0,
			WriteByteTimeout:           0,
			CountError:                 nil,
		}}

		re, err = marc21.NewRESTEngine(httpClient)
		if err != nil {
			logger.Fatal().Msgf("cannot initialize rest engine: %v", err)
		}
	}

	logger.Info().Msgf("rest services loaded")

	mapperFactory, err := marc21.NewMapperManager()
	if err != nil {
		logger.Fatal().Msgf("cannot initialize mapper factory: %v", err)
	}
	mapperFunctionScalar, err := marc21.NewMapperFunctionScalar()
	if err != nil {
		logger.Fatal().Msgf("cannot initialize mapper function scalar: %v", err)
	}
	mapperFactory.Add(mapperFunctionScalar)
	mapperFunctionJS, err := marc21.NewMapperFunctionJS(jse)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize mapper function JS: %v", err)
	}
	mapperFactory.Add(mapperFunctionJS)
	mapperFunctionREST, err := marc21.NewMapperFunctionREST(re)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize mapper function REST: %v", err)
	}
	mapperFactory.Add(mapperFunctionREST)
	mapperFunctionINT, err := marc21.NewMapperFunctionInt()
	if err != nil {
		logger.Fatal().Msgf("cannot initialize mapper function INT: %v", err)
	}
	mapperFactory.Add(mapperFunctionINT)

	fieldMapper, err := marc21.NewMapper(mapperFactory, jse, re)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize field mapper: %v", err)
	}
	facetMapper, err := marc21.NewMapper(mapperFactory, jse, re)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize facet mapper: %v", err)
	}
	aclMapper, err := marc21.NewMapper(mapperFactory, jse, re)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize facet mapper: %v", err)
	}

	flagMapper, err := marc21.NewMapper(mapperFactory, jse, re)
	if err != nil {
		logger.Fatal().Msgf("cannot initialize flag mapper: %v", err)
	}

	for _, table := range conf.ConfigTable {
		if conf.JSDir == "" {
			jsList := map[string]string{}
			if err := gSheet.LoadJSConfig(jsList, table, conf.ConfigJS); err != nil {
				logger.Fatal().Msgf("cannot load javascript sheet from %s:%s from google", conf.ConfigTable, conf.ConfigJS)
			}
			for name, script := range jsList {
				if err := jse.AddScript(name, script); err != nil {
					logger.Fatal().Msgf("cannot add %s:%s/%s to js cache: %v", conf.ConfigTable, conf.ConfigJS, name, err)
				}
			}
		}
		if conf.EnableServices {
			serviceList := map[string]marc21.ServiceConfig{}
			if err := gSheet.LoadServiceConfig(serviceList, table, conf.ConfigService); err != nil {
				logger.Fatal().Msgf("cannot load rest services sheet from %s:%s from google", conf.ConfigTable, conf.ConfigService)
			}
			for name, service := range serviceList {
				if err := re.AddCache(name, service); err != nil {
					logger.Fatal().Msgf("cannot add %s:%s/%s to REST cache: %v", conf.ConfigTable, conf.ConfigService, name, err)
				}
			}
		}
		if err := gSheet.LoadMapperConfig(fieldMapper, table, conf.ConfigMapping); err != nil {
			logger.Fatal().Msgf("cannot load field config sheet %s:%s from google: %v", conf.ConfigTable, conf.ConfigMapping, err)
			return
		}

		if err := gSheet.LoadMapperConfig(facetMapper, table, conf.ConfigFacet); err != nil {
			logger.Fatal().Msgf("cannot load sheet %s:%s from google: %v", conf.ConfigTable, conf.ConfigFacet, err)
			return
		}

		if err := gSheet.LoadMapperConfig(aclMapper, table, conf.ConfigACL); err != nil {
			logger.Fatal().Msgf("cannot load sheet %s:%s from google: %v", conf.ConfigTable, conf.ConfigACL, err)
			return
		}

		if err := gSheet.LoadFlagConfig(flagMapper, table, conf.ConfigFlag); err != nil {
			logger.Fatal().Msgf("cannot load sheet %s:%s from google: %v", conf.ConfigTable, conf.ConfigFlag, err)
			return
		}
	}

	logger.Info().Msgf("mapper initialized")

	jse.Startup()
	logger.Info().Msgf("javascript engine started")

	var indexer bulk.Indexer
	if conf.ElasticV8 {
		indexer, err = bulk.NewIndexer8(conf.ElasticEndpoint, conf.ElasticIndex, string(conf.ElasticAPIKey), logger)
		if err != nil {
			logger.Fatal().Msgf("cannot initialize elastic client")
		}
	} else {
		indexer, err = bulk.NewIndexer7(conf.ElasticEndpoint, conf.ElasticIndex, logger)
		if err != nil {
			logger.Fatal().Msgf("cannot initialize elastic client")
		}
	}

	typeClient, err := elasticsearch.NewTypedClient(elasticsearch.Config{
		APIKey:    string(conf.ElasticAPIKey),
		Addresses: conf.ElasticEndpoint,
		Logger:    &elastictransport.ColorLogger{Output: os.Stdout},
	})
	if err != nil {
		logger.Fatal().Msgf("cannot initialize typed elastic client")
	}
	logger.Info().Msgf("elastic client initialized")

	localFields := LocalFields{}
	for _, field := range conf.LocalFields {
		parts := strings.Split(field, ":")
		if len(parts) == 0 {
			continue
		}
		lf := &LocalField{
			Tag: parts[0],
		}
		if len(parts) > 1 {
			lf.SubFieldCode = parts[1]
		}
		if len(parts) > 2 {
			lf.SubFieldText = strings.ToLower(parts[2])
		}
		localFields = append(localFields, lf)
	}

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	// initial queue pool
	mapperQ := queue.NewPool(int(conf.MapperWorkers), queue.WithQueueSize(int(conf.MapperQueueSize)))
	// shutdown the service and notify all the worker
	// wait all jobs are complete.
	defer mapperQ.Release()

	baseURL := metha.PrependSchema(conf.Endpoint)

	cVersion, sVersion, err := indexer.Info()
	if err != nil {
		logger.Error().Msgf("cannot get elastic info: %v", err)
	}
	// Print client and server version numbers.
	logger.Info().Msgf("Elasticsearch Client: %s", cVersion)
	logger.Info().Msgf("Elasticsearch Server: %s", sVersion)
	logger.Info().Msgf(strings.Repeat("~", 37))

	if conf.ElasticIndexCreate != "" /* && (_single == nil || *_single == "" )*/ {
		logger.Info().Msgf("loading schema '%s'", conf.ElasticIndexCreate)
		schemabytes, err := os.ReadFile(conf.ElasticIndexCreate)
		if err != nil {
			logger.Fatal().Msgf("cannot read schema %s", conf.ElasticIndexCreate)
		}
		if err := indexer.CreateIndex(schemabytes); err != nil {
			logger.Fatal().Msgf("Cannot delete index: %s", err)
		}
		conf.EndpointStart.Time = time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	}

	var emb *embedding.Embedding
	if len(conf.Embedding.Flags) > 0 {
		logger.Debug().Msgf("embedding enabled")
		tplFile, err := os.ReadFile(conf.Embedding.Template)
		if err != nil {
			logger.Fatal().Msgf("cannot read template %s: %v", conf.Embedding.Template, err)
		}
		tpl, err := template.New(conf.Embedding.Template).Parse(string(tplFile))
		if err != nil {
			logger.Fatal().Msgf("cannot parse template %s: %v", conf.Embedding.Template, err)
		}
		badgerDB, err := badger.Open(badger.DefaultOptions(conf.Embedding.CacheFolder))
		if err != nil {
			logger.Fatal().Msgf("cannot open badger db %s: %v", conf.Embedding.CacheFolder, err)
		}
		emb = embedding.NewEmbedding(tpl, conf.Embedding.Flags, badgerDB, string(conf.Embedding.OpenaiApiKey), logger)
	}

	var docNum uint

	//	var start = time.Now()

	harvest, err := oaipmh.NewHarvester(
		baseURL,
		"marc21",
		conf.EndpointSet,
		200000,
		1,
		time.Duration(0),
		logger)
	if err != nil {
		logger.Fatal().Err(err)
	}

	//var fields = map[string]int64{}

	//2022-09-07T16:24:01Z
	if err := indexer.StartBulk(int(conf.IndexerWorkers), int(5e+6), 30*time.Second); err != nil {
		logger.Fatal().Msgf("Error creating the indexer: %s", err)
	}
	defer indexer.CloseBulk()

	var mappingWaiter = &sync.WaitGroup{}
	var mappingProcesses uint64 = 0
	var counter uint64 = 0
	var maxProcesses = uint64(float64(conf.MapperQueueSize) * 0.8)
	if uint64(conf.MapperWorkers)*10 < maxProcesses {
		maxProcesses = uint64(conf.MapperWorkers) * 10
	}

	doit := func() bool {
		//		var start = time.Now()
		var from = conf.EndpointStart.Time.UTC()

		if conf.LastSyncInGoogleSheet {
			lastSyncSheet, err := gconfig.NewGSheet(gconf)
			if err != nil {
				logger.Fatal().Msgf("cannot connect to google sheets: %v", err)
			}
			lastSync, err := lastSyncSheet.LoadLastSyncValue(conf.LastSyncGoogleSheetID, conf.LastSyncName, logger)
			if err != nil {
				logger.Fatal().Msgf("cannot load last sync value from google sheet: %v", err)
			}
			if lastSync != "" {
				from, err = time.Parse(SYNCTIMEFORMAT, lastSync)
				if err != nil {
					logger.Fatal().Msgf("invalid last sync time: %s", lastSync)
				}
				logger.Info().Msgf("last sync time loaded from sheet for name %s: %s", conf.LastSyncName, from)
			}
		} else if conf.LastsyncFile != "" {
			_, err := os.Stat(conf.LastsyncFile)
			if !os.IsNotExist(err) {
				fromData, err := os.ReadFile(conf.LastsyncFile)
				if err != nil {
					logger.Fatal().Msgf("cannot read endpoint start '%s': %v", conf.LastsyncFile, err)
				}
				from, err = time.Parse(SYNCTIMEFORMAT, string(fromData))
				currentTimeZone, offset := from.Zone()
				logger.Info().Msgf("The start time zone is: %s", currentTimeZone)
				logger.Info().Msgf("Time start zone offset: %s", offset)

				if err != nil {
					logger.Fatal().Msgf("invalid endpoint-start format: '%s'", conf.LastsyncFile)
				}
			} else {
				//logger.Fatal().Msgf("invalid endpoint-start file '%s': %v", conf.LastsyncFile, err)
			}
		} else {
			log.Fatalf("No valid sync option defined. Either set LastSyncInGoogleSheet to true or define a LastsyncFile.")
		}

		indexerFunc := func(_indexer bulk.Indexer, _rec metha.Record, _num int64) {
			//logger.Info().Msgf("docID: %s", _rec.Header.Identifier)
			daFunction := func(ctx context.Context) error {
				defer func() {
					atomic.AddUint64(&mappingProcesses, ^uint64(0))
					mappingWaiter.Done()
				}()

				var docID = _rec.Header.Identifier
				var oaiID = _rec.Header.Identifier

				var marcRec = &marc21.Record{
					OAIID: []string{oaiID},
				}

				if _rec.Header.Status == "deleted" {
					if conf.IDField == "" {
						if err := indexer.Delete(docID); err != nil {
							logger.Warn().Msgf("Unexpected error deleting %s: %v", docID, err)
						}
						return nil
					}

					// search the document
					resp, err := typeClient.Search().
						Index(conf.ElasticIndex).
						Query(
							&types.Query{
								Match: map[string]types.MatchQuery{
									"oai_id": types.MatchQuery{Query: oaiID},
								},
							},
						).
						Do(context.Background())
					if err != nil {
						logger.Error().Err(err).Msgf("cannot get document '%s' for deletion", oaiID)
						return errors.Wrapf(err, "cannot get document '%s' for deletion", oaiID)
					}
					// if not found, do nothing
					if len(resp.Hits.Hits) == 0 {
						logger.Debug().Msgf("cannot delete document '%s' - not found", oaiID)
						return nil
					}
					source_ := resp.Hits.Hits[0].Source_
					_docID := resp.Hits.Hits[0].Id_
					if _docID == nil {
						logger.Error().Msgf("hit %s has nil id", oaiID)
					}
					docID := *_docID
					index := resp.Hits.Hits[0].Index_
					if !conf.Deduplicate {
						if err := indexer.Delete(docID); err != nil {
							logger.Warn().Msgf("Unexpected error deleting %s: %v", docID, err)
						}
						return nil
					}

					var doc = map[string]any{}
					if err := json.Unmarshal(source_, &doc); err != nil {
						logger.Error().Err(err).Msgf("cannot unmarshal document '%s'", docID)
						return errors.Wrapf(err, "cannot unmarshal document '%s'", docID)
					}

					mgDoc, err := elasticGetDoc2Struct(doc)
					mgDoc.ID = docID
					mgDoc.Index = index
					if err != nil {
						return errors.Wrapf(err, "cannot map document '%v'", doc)
					}

					oais := []string{}
					for _, o := range mgDoc.OAIId {
						if o != oaiID {
							oais = append(oais, o)
						}
					}
					// falls keine weiteren OAIId's vorhanden, dann Datensatz löschen
					if len(oais) == 0 {
						if err := _indexer.Delete(mgDoc.ID); err != nil {
							logger.Error().Err(err).Msgf("cannot delete document '%s'", mgDoc.ID)
							return errors.Wrapf(err, "cannot delete document '%s'", mgDoc.ID)
						}
						return nil
					}
					marcRec := &marc21.Record{
						OAIID:      oais,
						Datafields: mgDoc.Datafields,
						Leader:     mgDoc.Leader,
					}
					marcRec.Datafields = removeLocalFields(localFields, conf.LocalName, marcRec.Datafields)
					if err := marcRec.Optimize([]string{conf.EndpointSet}); err != nil {
						logger.Error().Err(err).Msgf("cannot optimize record '%s'", mgDoc.ID)
						return errors.Wrapf(err, "cannot optimize record '%s'", mgDoc.ID)
					}
					if err := marcRec.Map(fieldMapper, facetMapper, flagMapper, aclMapper); err != nil {
						logger.Error().Err(err).Msgf("error mapping document '%s'", docID)
						//return errors.Wrapf(err, "error mapping '%s'", _docID)
						return nil
					}
					//logger.Info().Msgf("[%05d] indexing %s", _num, docID)
					if err := _indexer.Index(mgDoc.ID, marcRec); err != nil {
						logger.Error().Err(err).Msgf("error indexing document '%s'", docID)
						return nil
					}
					return nil
				}

				buf := bytes.NewBuffer(_rec.Metadata.Body)
				dec := xml.NewDecoder(buf)
				if err = dec.Decode(marcRec); err != nil {
					return errors.Wrapf(err, "cannot parse metadata: %s", _rec.Metadata.GoString())
				}
				if conf.IDField != "" {
					fld035 := marcRec.GetDatafields("035")
					if len(fld035) > 0 {
						for _, fld := range fld035 {
							for _, sub := range fld.Subfields {
								if sub.Code == "a" && strings.HasPrefix(sub.Text, conf.IDField) {
									docID = "id" + sub.Text[len(conf.IDField):]
									break
								}
							}
						}
					}
				}

				if conf.GoogleOnly {
					fld856 := marcRec.GetDatafields("856")
					found := false
					for _, fld := range fld856 {
						for _, sub := range fld.Subfields {
							if sub.Code == "q" && strings.TrimSpace(strings.ToLower(sub.Text)) == "google" {
								found = true
								break
							}

						}
					}
					if !found {
						// logger.Info().Msgf("skipping %s - google only", docID)
						return nil
					}
				}

				if conf.Deduplicate {
					var source_ []byte

					resp, err := typeClient.Get(conf.ElasticIndex, docID).Do(context.Background())
					if err != nil {
						return errors.Wrapf(err, "cannot get document '%s'", docID)
					}
					source_ = resp.Source_
					if len(source_) > 0 {
						logger.Info().Msgf("found %s", docID)
						var doc = map[string]any{}
						if err := json.Unmarshal(source_, &doc); err != nil {
							return errors.Wrapf(err, "cannot unmarshal document '%s'", docID)
						}

						mgDoc, err := elasticGetDoc2Struct(doc)
						if err != nil {
							return errors.Wrapf(err, "cannot map document '%v'", doc)
						}
						mgDoc.Found = true
						mgDoc.ID = resp.Id_
						mgDoc.Index = resp.Index_
						marcRec.OAIID = append(marcRec.OAIID, mgDoc.OAIId...)
						slices.Sort(marcRec.OAIID)
						marcRec.OAIID = slices.Compact(marcRec.OAIID)
						logger.Info().Msgf("OAI IDs %v", marcRec.OAIID)
						if len(marcRec.OAIID) > 1 {
						}
						marcRec.Datafields = setLocalFields(localFields, conf.LocalName, marcRec.Datafields)
						if mgDoc.Found {
							newFields := combineDatafields(localFields, conf.LocalName, mgDoc.Datafields, marcRec.Datafields)
							marcRec.Datafields = newFields
						}
					} else {
						marcRec.Datafields = setLocalFields(localFields, conf.LocalName, marcRec.Datafields)
					}
				} else {
					marcRec.Datafields = setLocalFields(localFields, conf.LocalName, marcRec.Datafields)
				}
				if err := marcRec.Optimize(_rec.Header.SetSpec); err != nil {
					logger.Error().Err(err)
					return errors.WithStack(err)
				}

				// logger.Info().Msgf("mapping %s", _docID)
				if err := marcRec.Map(fieldMapper, facetMapper, flagMapper, aclMapper); err != nil {
					logger.Error().Msgf("error mapping document '%s': %v", docID, err)
					//return errors.Wrapf(err, "error mapping '%s'", _docID)
					return nil
				}

				var doEmbedding = false
				if len(conf.Embedding.Flags) > 0 {
					if emb == nil {
						logger.Error().Msgf("embedding requested but not initialized")
						return errors.New("embedding requested but not initialized")
					}
					for _, flag := range conf.Embedding.Flags {
						if slices.Contains(marcRec.Flags, flag) {
							doEmbedding = true
							break
						}
						if doEmbedding {
							break
						}
					}
					if doEmbedding {
						logger.Info().Msgf("embedding %s", docID)
						embeddings, err := emb.GetEmbedding(marcRec)
						if err != nil {
							logger.Error().Err(err).Msgf("cannot get embedding for %s", docID)
							return errors.Wrapf(err, "cannot get embedding for %s", docID)
						}
						var ok bool
						marcRec.EmbeddingProse, ok = embeddings["prose"]
						marcRec.EmbeddingMarc, ok = embeddings["marc"]
						marcRec.EmbeddingJSON, ok = embeddings["json"]
						_ = ok
					} else {
						//logger.Info().Msgf("no embedding %s", docID)
					}
				}

				//logger.Info().Msgf("[%05d] indexing %s", _num, docID)
				if err := _indexer.Index(docID, marcRec); err != nil {
					logger.Error().Msgf("error indexing document '%s': %v", docID, err)
					return nil
				}
				//					logger.Info().Msgf("done %s", _docID)
				return nil
			}
			err := mapperQ.QueueTask(daFunction)
			if err != nil {
				logger.Fatal().Msgf("cannot map record %s: %v", _rec.Header.Identifier, err)
			}
		}

		if _single != nil && *_single != "" {
			logger.Info().Msgf("getting single record '%s'", *_single)
			rec, err := harvest.GetRecord(*_single)
			if err != nil {
				logger.Fatal().Msgf("cannot get record '%s': %v", *_single, err)
			}
			mappingWaiter.Add(1)
			indexerFunc(indexer, rec, 0)
			mappingWaiter.Wait()
			return false
		}

		untilTime := time.Now()

		logger.Info().Msgf("starting harvest from %s until %s", from.UTC().Format(SYNCTIMEFORMAT), untilTime.UTC().Format(SYNCTIMEFORMAT))
		harvest.Harvest(from.UTC(), untilTime.UTC(), func(recs []metha.Record) error {
			for _, oaiRecord := range recs {
				for atomic.LoadUint64(&mappingProcesses) >= maxProcesses {
					logger.Info().Msgf("%d tasks in queue. waiting", atomic.LoadUint64(&mappingProcesses))
					time.Sleep(1 * time.Second)
					//atomic.SwapUint64(&mappingProcesses, 0)
				}
				counter++
				mappingWaiter.Add(1)
				val := atomic.AddUint64(&mappingProcesses, 1)
				/*
					for atomic.LoadUint64(&mappingProcesses) >= uint64(conf.MapperWorkers) {
						time.Sleep(time.Second)
					}
				*/
				if counter%100 == 0 {
					logger.Debug().Msgf("adding %07d - %d tasks in queue - %s doc/sec",
						counter,
						val,
						humanize.Comma(int64(1000.0/float64(time.Now().Sub(untilTime)/time.Millisecond)*float64(counter))),
					)
				}
				indexerFunc(indexer, oaiRecord, int64(counter))
				docNum++
				if conf.MaxDoc > 0 && docNum > conf.MaxDoc {
					logger.Error().Msgf("maximum number of %v documents reached", docNum)
					return errors.Errorf("maximum number of %v documents reached", docNum)
				}
			}
			return nil
		})
		logger.Info().Msgf("Wait for all processing for harvested records to complete.")
		mappingWaiter.Wait()
		logger.Info().Msgf("Completed all mappings for harvested records.")
		if mapperQ.FailureTasks() != 0 {
			logger.Error().Msgf("Som mappings failed: %d", mapperQ.FailureTasks())
		}
		if conf.LastSyncInGoogleSheet {
			if err = gSheet.SetLastSyncValue(conf.LastSyncGoogleSheetID, conf.LastSyncName, untilTime.Format(SYNCTIMEFORMAT), logger); err != nil {
				logger.Fatal().Msgf("cannot set last sync value in google sheet: %v", err)
			}
		} else if conf.LastsyncFile != "" {
			if err = os.WriteFile(conf.LastsyncFile, []byte(untilTime.Format(SYNCTIMEFORMAT)), 0666); err != nil {
				logger.Fatal().Msgf("cannot write to file '%s': %v", conf.LastsyncFile, err)
			}
		}
		return true
	}
	if !doit() {
		return
	}

	if len(conf.Cron.Hours) > 0 {
		done := make(chan os.Signal, 1)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			defer wg.Done()
			slices.Sort(conf.Cron.Hours)
			maxHour := slices.Max(conf.Cron.Hours)
			for {
				now := time.Now()
				loc := now.Location()
				minute := now.Minute()
				hour := now.Hour()
				year := now.Year()
				month := now.Month()
				day := now.Day()
				if hour > maxHour || (hour == maxHour && minute >= conf.Cron.Minute) {
					now = now.Add(time.Hour * 24)
					year = now.Year()
					month = now.Month()
					day = now.Day()
				}
				var next time.Time
				for _, h := range conf.Cron.Hours {
					next = time.Date(year, month, day, h, conf.Cron.Minute, 0, 0, loc)
					if now.Before(next) {
						break
					}
				}
				fmt.Printf("next sync: %v", next)
				select {
				case <-time.After(next.Sub(now)):
					doit()
				case sig := <-done:
					fmt.Printf("got signal %v\n", sig)
					return
				}
			}
		}()
		signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
		fmt.Println("press ctrl+c to stop server")
		wg.Wait()
	}
}
