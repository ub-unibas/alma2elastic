FROM golang:1.23 as builder

ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.switch.ch/ub-unibas/*
ENV CGO_ENABLED=1
ENV GOOS=linux
ENV GOARCH=amd64

WORKDIR /source
COPY . /source
RUN go mod download
WORKDIR /source/cmd/alma2elastic
RUN go build -o /app/alma2elastic
WORKDIR /source/cmd/dsv4ever
RUN go build -o /app/dsv4ever

FROM debian:latest
WORKDIR /
COPY --from=builder /app /app

ENTRYPOINT [ "/app/alma2elastic" ]