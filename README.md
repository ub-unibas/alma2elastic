# Alma 2 Elastic

## Development Requirements

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Go](https://go.dev/doc/install)
- [Docker Engine](https://docs.docker.com/engine/install/)

The project requires a c-compiler on the system as it creates cgo bindings for the v8 support. Install build-essential on your system

For Ubuntu:
```shell
sudo apt-get update
sudo apt-get install build-essential
```

## Install Dependencies
With this command all the dependencies are installed for this project.

```shell
go get -u ./cmd/alma2elastic
```

## Build Docker Image
Use this command to build the image locally. For production environments it is expected to run this container from the
image generated for the GitLab container registry.

It is necessary to install docker and have the user in the sudo docker group in order to build an image.

In order to integrate the latest changes in the docker image it is necessary to create a go release of the packages. To
do so a git tag has to be added and pushed to the remote. The docker build process always loads the latest git tag when installing
the go package.

```shell
docker build -t test-service .
```

## dsv4ever
This entry point loads the dsv01 and dsv05 dump, transform the data to json and extracts the field list. Then all the data
is indexed in 