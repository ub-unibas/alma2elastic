# Internal Functions
Alma2Elastic provides several internal functions which are used whenever the whole MARC record instead of just a single field has to be taken into account for the mapping or if simple enrichments have to be performed.

## Date values (fields 008 and 046)
Internal function: [mapperFunctionIntDates](https://gitlab.switch.ch/ub-unibas/alma2elastic/-/blob/main/pkg/marc21/mapperFunctionIntDates.go?ref_type=heads)

The internal function for processing date values returns a JSON object with date.gte and date.lte. This can be indexed as a field of type [date](https://www.elastic.co/guide/en/elasticsearch/reference/current/date.html) and as a field of type [daterange](https://www.elastic.co/guide/en/elasticsearch/reference/current/range.html).

The following rules apply:
* If field 046 exists and a date can be read from it, it is used. Otherwise, the values from 008/07-14 are used.
* If the first date (008/07-10) contains "u", the character is replaced by "0". The value "0000" is not returned.
* If the second date (008/11-14) contains "u", the character is replaced by "9". The value "9999" is not returned.
* For type of date (008/06) "m", a second year (008/11-14) "9999" is not returned.
* For type of date (008/06) "c", "d", "i", "k", "u" and "q", a second year (008/11-14) "9999" is replaced by the current year.
* For type of date (008/06) "r", the first year (008/07-10) is written in date.gte and the second year (008/11-14) is written in date.lte. A second year "9999" is not indexed. No date range is created.
* If there is only one date value, it is written to date.gte and date.lte and a date range is created.
* Date values are currently indexed in mapping.date.\* as field of type [date](https://www.elastic.co/guide/en/elasticsearch/reference/current/date.html) and in mapping.daterange as fields of type [daterange](https://www.elastic.co/guide/en/elasticsearch/reference/current/range.html).


## Holdings and Items (fields 852, 866 and 949)
Internal function: [mapperFunctionIntHolding](https://gitlab.switch.ch/ub-unibas/alma2elastic/-/blob/main/pkg/marc21/mapperFunctionIntHolding.go)

Holdings and item data are added to the bibliographic record during export by Alma (see [Alma OAI-Publishing](https://ub-basel.atlassian.net/wiki/x/BAC4hg) for configuration recommendations).
Holdings data is exported in fields 852 and 866. Item data is exported in field 949. The fields all contain the holding identifier. Based on this identifier, Alma2elastic merges the fields into a JSON object.


Example - Input (Holding ID in $8 of 852 and 866 and in $3 of 949):
```
{
  "tag": "852",
  "ind1": "4",
  "ind2": " ",
  "subfield": [
    "|b A140",
    "|c 140ZM",
    "|j UBM Med Zs 836",
    "|x Akz: ba/uub-/g",
    "|8 22247441120005504"
  ]
},
{
  "tag": "866",
  "ind1": " ",
  "ind2": " ",
  "subfield": [
    "|a [Jahrgänge 2005-2013]",
    "|z * [Bezug eingestellt]",
    "|8 22247441120005504"
  ]
},
{
  "tag": "949",
  "ind1": " ",
  "ind2": " ",
  "subfield": [
    "|3 22247441120005504",
    "|o ISSBD",
    "|x i/2013/A280",
    "|2 A1001850986",
    "|c 140ZM",
    "|x + CD-ROM",
    "|h UBM Med Zs 836:155:593/1218",
    "|1 23247441090005504",
    "|q Bd. 155 (2007), p. 593/1218 & Suppl./CD-ROM",
    "|p 23",
    "|j UBM Med Zs 836",
    "|b A140",
    "|9 UBS"
  ]
},
{
  "tag": "949",
  "ind1": " ",
  "ind2": " ",
  "subfield": [
    "|3 22247441120005504",
    "|o ISSBD",
    "|x i/2013/A280",
    "|2 A1001850984",
    "|c 140ZM",
    "|x + CD-ROM",
    "|h UBM Med Zs 836:153",
    "|1 23247441110005504",
    "|q Bd. 153 (2005) & CD-ROM",
    "|p 23",
    "|j UBM Med Zs 836",
    "|b A140",
    "|9 UBS"
  ]
},
{
  "tag": "949",
  "ind1": " ",
  "ind2": " ",
  "subfield": [
    "|3 22247441120005504",
    "|o ISSBD",
    "|2 A1001944627",
    "|c 140ZM",
    "|z Nr. 3 + 4 fehlen! Nr. 1 + 7 separat gebunden!",
    "|h UBM Med Zs 836:154:2/5/6",
    "|1 23247440970005504",
    "|q Bd. 154 (2006), No. 2, 5 + 6",
    "|p 23",
    "|j UBM Med Zs 836",
    "|b A140",
    "|9 UBS"
  ]
}
```

Example - Output:
```
{
  "library": "A140",
  "location": "140ZM",
  "callNumber": "UBM Med Zs 836",
  "summary": "[Jahrgänge 2005-2013]",
  "summaryNote": ", * [Bezug eingestellt]"
  "items": [
    {
      "itemCallNumber": "UBM Med Zs 836:155:593/1218"
    },
    {
      "itemCallNumber": "UBM Med Zs 836:153"
    },
    {
      "itemCallNumber": "UBM Med Zs 836:154:2/5/6",
      "note": "Nr. 3 + 4 fehlen! Nr. 1 + 7 separat gebunden!"
    }
  ]
}

```