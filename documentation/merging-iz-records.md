# Merging records from several IZs of Alma

Alma2Elastic can be configured to process data from several Institution Zones (IZ) of Alma and to merge the records
based on the Network Zone (NZ) identifier.

A set of local fields which will be added from each IZ record can be configured. To each local field a subfield $9 will
be added containing a name for the source. The value for the name of the source can be configured.

The merged record with all added local fields is the input for all further processing, i.e. mapping, enrichment, by Alma2Elastic.

## Configuration

### deduplicate

Set to true if records from different IZs should be merged.

```
deduplicate = true
```

### idfield

The identifier from field 035 $a which is used to merge the records. This identifier will be used as the identifier for
the resulting document.

For SLSP NZ use:

```
idfield = "(EXLNZ-41SLSP_NETWORK)"
```

### localfields

A list of fields which have to be added from all records to the merged document.
Local extensions are exported from Alma with a subfield $9 local or $9 LOCAL. By adding "9:9:local" to the list of
fields, all local extensions will be added to the merged document.

Recommended use:

```
localfields = ["852", "866", "947", "948", "949", "AVD", "9:9:local"]
```

With this configuration all fields 852, 866, 947, 948, 949 and AVD are added. Additionally, all local extensions are
added.

### localname

A name for the source. This name is added in a subfield $9 to all the local fields which were added from this source.

For SLSP, use the code for the IZ:

```
localname = "UBS"
```

## Example for a merged document

* uses the NZ MMS ID in "_id"
* contains all OAI identifiers in "oai_id"
* contains one IZ MMS ID in "controlfield.001"
* contains the configured localfields from all IZs, including subfield $9 with the name configured in localname

```
"_id": "id991000131799705501",
  "_source": {
    "timestamp": "2024-03-13T09:17:31Z",
    "oai_id": [
      "oai:alma.41SLSP_UBS:9927848720105504",
      "oai:alma.41SLSP_UZB:99117188310705508"
    ],
    "LDR": {
      "leader_06_typeOfRecord": "a",
      "leader_07_bibliographicStatus": "m",
      "leader_17_encodingLevel": "2",
      "leader_19_multipartLevel": " ",
      "leader_full": "01463nam a22003492c 4500"
    },
    "controlfield": {
      "001": "99117188310705508",
      "005": "20220909163140.0",
      "008": "030402s1804    gw            00| | ger  ",
      "008_06_typeOfDate": "s",
      "008_07-10_dateFirst": "1804",
      "008_11-14_dateSecond": "    ",
      "008_15-17_country": "gw ",
      "008_23or29_formOfItem": " ",
      "008_35-37_language": "ger"
    },
    "datafield": [
      {
        "tag": "035",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a (swissbib)042924006-41slsp_network"
        ]
      },
      {
        "tag": "035",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a 042924006",
          "|9 ExL"
        ]
      },
      {
        "tag": "035",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a (IDSBB)002784872DSV01"
        ]
      },
      {
        "tag": "035",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a (EXLNZ-41SLSP_NETWORK)991020443539705501"
        ]
      },
      {
        "tag": "040",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a SzZuIDS BS/BE A100",
          "|b ger",
          "|e rda",
          "|d CH-ZuSLS"
        ]
      },
      {
        "tag": "041",
        "ind1": "0",
        "ind2": " ",
        "subfield": [
          "|a ger"
        ]
      },
      {
        "tag": "082",
        "ind1": "7",
        "ind2": "4",
        "subfield": [
          "|a 220",
          "|2 23sdnb"
        ]
      },
      {
        "tag": "245",
        "ind1": "0",
        "ind2": "0",
        "subfield": [
          "|a <<Die>> Bibel, oder die ganze Heilige Schrift des alten und neuen Testaments",
          "|b nach der deutschen Uebersetzung D. Martin Luthers"
        ]
      },
      {
        "tag": "250",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a Die CVII. Auflage"
        ]
      },
      {
        "tag": "264",
        "ind1": " ",
        "ind2": "1",
        "subfield": [
          "|a Halle",
          "|b in der Cansteinischen Bibel-Anstalt",
          "|c 1804"
        ]
      },
      {
        "tag": "300",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a 14 Seiten, 1 ungezähltes Blatt, 1079 Seiten, 1 ungezählte Seite; 308 Seiten, 2 ungezählte Blätter",
          "|c 8'"
        ]
      },
      {
        "tag": "336",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|b txt",
          "|2 rdacontent"
        ]
      },
      {
        "tag": "337",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|b n",
          "|2 rdamedia"
        ]
      },
      {
        "tag": "338",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|b nc",
          "|2 rdacarrier"
        ]
      },
      {
        "tag": "500",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a Mit Apokryphen"
        ]
      },
      {
        "tag": "500",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a Neues Testament mit eigenem Titelblatt und abweichendem Impressum: \"Halle, zu finden im Waisenhause, 1804\""
        ]
      },
      {
        "tag": "505",
        "ind1": "0",
        "ind2": "0",
        "subfield": [
          "|t <<Das>> Neue Testament unsers Herrn und Heilandes Jesu Christi, verdeutscht von D. Martin Luthern"
        ]
      },
      {
        "tag": "700",
        "ind1": "1",
        "ind2": " ",
        "subfield": [
          "|a Luther, Martin",
          "|d 1483-1546",
          "|0 (DE-588)118575449",
          "|4 trl"
        ]
      },
      {
        "tag": "710",
        "ind1": "2",
        "ind2": " ",
        "subfield": [
          "|a Cansteinsche Bibelanstalt",
          "|0 (DE-588)16032021-5",
          "|4 prt"
        ]
      },
      {
        "tag": "730",
        "ind1": "0",
        "ind2": " ",
        "subfield": [
          "|a Bibel",
          "|0 (DE-588)4006406-2"
        ]
      },
      {
        "tag": "751",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a Halle (Saale)",
          "|0 (DE-588)4023025-9",
          "|4 pup"
        ]
      },
      {
        "tag": "900",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a IDSIxunikat"
        ]
      },
      {
        "tag": "900",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a NOMERGEALEX"
        ]
      },
      {
        "tag": "900",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a OK_GND",
          "|x UZB/Z01/202205/schu"
        ]
      },
      {
        "tag": "900",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a Stoppsignal FRED",
          "|x UZB/Z01/202205"
        ]
      },
      {
        "tag": "990",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|a Z01FE202204rflysk",
          "|9 local",
          "|9 UZB"
        ]
      },
      {
        "tag": "990",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|d Z01SE202205kschu",
          "|9 local",
          "|9 UZB"
        ]
      },
      {
        "tag": "852",
        "ind1": "4",
        "ind2": " ",
        "subfield": [
          "|b Z06",
          "|c RAM",
          "|j AWA 3147",
          "|8 22497005970005508",
          "|9 UZB"
        ]
      },
      {
        "tag": "949",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|o BOOK",
          "|2 ZM03680024",
          "|c RAM",
          "|1 23497005950005508",
          "|p 12",
          "|j AWA 3147",
          "|b Z06",
          "|9 UZB"
        ]
      },
      {
        "tag": "990",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|f akmedea",
          "|9 LOCAL",
          "|9 UBS"
        ]
      },
      {
        "tag": "852",
        "ind1": "4",
        "ind2": " ",
        "subfield": [
          "|b A100",
          "|c MAG",
          "|j UBH BibG C 232",
          "|8 22207367820005504",
          "|9 UBS"
        ]
      },
      {
        "tag": "949",
        "ind1": " ",
        "ind2": " ",
        "subfield": [
          "|3 22207367820005504",
          "|o BOOK",
          "|x 00083559",
          "|2 DSVN640899",
          "|c MAG",
          "|1 23207367810005504",
          "|x RMedea",
          "|p 11",
          "|j UBH BibG C 232",
          "|b A100",
          "|9 UBS"
        ]
      }
    ]
  },
```