package oaipmh

import (
	"emperror.dev/errors"
	"fmt"
	"github.com/je4/utils/v2/pkg/zLogger"
	"github.com/miku/metha"
	"net/http"
	"time"
)

type Harvester struct {
	BaseURL           string
	Format            string
	Set               string
	From              string
	Until             string
	Client            *metha.Client
	Identify          *metha.Identify
	ExtraHeaders      http.Header
	EarliestDate      time.Time
	MaxEmptyResponses int
	MaxRequests       int
	Delay             time.Duration
	logger            zLogger.ZLogger
}

func NewHarvester(endpoint, format, set string, maxRequests, maxEmptyResponses int, delay time.Duration, logger zLogger.ZLogger) (*Harvester, error) {
	h := &Harvester{
		BaseURL:           endpoint,
		Format:            format,
		Set:               set,
		MaxRequests:       maxRequests,
		MaxEmptyResponses: maxEmptyResponses,
		Delay:             delay,
		logger:            logger,
	}
	if err := h.Init(); err != nil {
		return nil, err
	}
	return h, nil
}

func (harvester *Harvester) Init() error {
	if err := harvester.identify(); err != nil {
		return errors.Wrap(err, "cannot identify")
	}
	if err := harvester.earliestDate(); err != nil {
		return errors.Wrap(err, "cannot get earliestDate")
	}
	return nil
}

func (harvester *Harvester) identify() error {
	req := metha.Request{
		Verb:         "Identify",
		BaseURL:      harvester.BaseURL,
		ExtraHeaders: harvester.ExtraHeaders,
	}
	if harvester.Client == nil {
		harvester.Client = metha.DefaultClient
	}
	resp, err := harvester.Client.Do(&req)
	if err != nil {
		return err
	}
	harvester.Identify = &resp.Identify
	return nil
}

// earliestDate returns the earliest date as a time.Time value.
// from metha/harvest (https://github.com/miku/metha/blob/master/harvest.go)
func (harvester *Harvester) earliestDate() (err error) {
	harvester.EarliestDate, err = harvester.toDate(harvester.Identify.EarliestDatestamp)
	return err
}

func (harvester *Harvester) toDate(timeStamp string) (time.Time, error) {
	var err error
	var result time.Time
	// Different granularities are possible: https://eudml.org/oai/OAIHandler?verb=Identify
	switch harvester.Identify.Granularity {
	case "YYYY-MM-DD":
		if len(timeStamp) <= 10 {
			result, err = time.Parse("2006-01-02", timeStamp)
			if err != nil {
				return time.Time{}, err
			}
		}
		result, err = time.Parse("2006-01-02", timeStamp[:10])
		if err != nil {
			return time.Time{}, err
		}
	case "YYYY-MM-DDThh:mm:ssZ":
		// refs. #8825
		if len(timeStamp) >= 10 && len(timeStamp) < 20 {
			result, err = time.Parse("2006-01-02", timeStamp[:10])
			if err != nil {
				return time.Time{}, err
			}
		}
		result, err = time.Parse("2006-01-02T15:04:05Z", timeStamp)
		if err != nil {
			return time.Time{}, err
		}
	default:
		return time.Time{}, fmt.Errorf("invalid earliestDatestamp - %s", timeStamp)
	}
	return result, nil
}

// DateLayout converts the repository endpoints advertised granularity to Go
// date format strings.
func (harvester *Harvester) DateLayout() string {
	switch harvester.Identify.Granularity {
	case "YYYY-MM-DD":
		return "2006-01-02"
	case "YYYY-MM-DDThh:mm:ssZ":
		return "2006-01-02T15:04:05Z"
	}
	return ""
}

func (harvester *Harvester) GetRecord(identifier string) (rec metha.Record, err error) {
	req := metha.Request{
		Verb:           "GetRecord",
		BaseURL:        harvester.BaseURL,
		Identifier:     identifier,
		ExtraHeaders:   harvester.ExtraHeaders,
		MetadataPrefix: harvester.Format,
	}
	resp, err := harvester.Client.Do(&req)
	if err != nil {
		return rec, err
	}
	return resp.GetRecord.Record, nil

}

func (harvester *Harvester) Harvest(from, until time.Time, fn func(rec []metha.Record) error) error {
	var token string
	var i, empty int
	for {
		if harvester.MaxRequests == i {
			harvester.logger.Info().Msgf("max requests limit (%d) reached\n", harvester.MaxRequests)
			break
		}

		req := metha.Request{
			BaseURL:                 harvester.BaseURL,
			MetadataPrefix:          harvester.Format,
			Verb:                    "ListRecords",
			Set:                     harvester.Set,
			ResumptionToken:         token,
			CleanBeforeDecode:       true,
			SuppressFormatParameter: false,
			ExtraHeaders:            nil,
			From:                    from.Format(harvester.DateLayout()),
			Until:                   until.Format(harvester.DateLayout()),
		}
		if harvester.Delay > 0 {
			time.Sleep(harvester.Delay)
		}
		resp, err := harvester.Client.Do(&req)
		if err != nil {
			return errors.Wrap(err, "cannot execute request")
		}
		// Handle OAI specific errors. XXX: An badResumptionToken kind of error
		// might be recoverable, by simply restarting the harvest.
		if resp.Error.Code != "" {
			// Rare case, where a resumptionToken is given, but it leads to
			// noRecordsMatch - we still want to save, whatever we got up until
			// this point, so we break here.
			switch resp.Error.Code {
			case "noRecordsMatch":
				if !resp.HasResumptionToken() {
					break
				}
				harvester.logger.Debug().Msg("resumptionToken set and noRecordsMatch, continuing")
			case "badResumptionToken":
				harvester.logger.Warn().Msg("badResumptionToken, might signal end-of-harvest")
				break
			case "InternalException":
				// #9717, InternalException Could not send Message.
				harvester.logger.Debug().Msg("InternalException: retrying request in a few instants...")
				time.Sleep(30 * time.Second)
				i++ // Count towards the total request limit.
				continue
			default:
				return resp.Error
			}
		}
		err = nil
		if len(resp.ListRecords.Records) > 0 {
			harvester.logger.Info().Msgf("Datestamp: %s\n", resp.ListRecords.Records[0].Header.DateStamp)
		}
		if err = fn(resp.ListRecords.Records); err != nil {
			break
		}

		/*
			datestamp := resp.ListRecords.Records[len(resp.ListRecords.Records)-1].Header.DateStamp
			harvester.logger.Info().Msgf("Datestamp: %s", datestamp)
		*/

		// Issue first observed at
		// https://gssrjournal.com/gssroai/?resumptionToken=33NjdYRs708&verb=ListRecords,
		// would spill the disk.
		prev := token
		if token = resp.GetResumptionToken(); token == "" {
			break
		}
		if prev == token {
			url, _ := req.URL()
			harvester.logger.Debug().Msgf("token %q did not change, assume server issue, moving to next window for: %s", token, url)
			break
		}
		i++
		if len(resp.ListRecords.Records) > 0 {
			empty = 0
		} else {
			empty++
			harvester.logger.Debug().Msgf("warning: successive empty response: %d/%d", empty, harvester.MaxEmptyResponses)
		}
		if empty == harvester.MaxEmptyResponses {
			harvester.logger.Debug().Msgf("max number of empty responses reached")
			break
		}

	}
	return nil
}
