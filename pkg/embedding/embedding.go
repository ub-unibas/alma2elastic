package embedding

import (
	"bytes"
	"emperror.dev/errors"
	"encoding/json"
	"fmt"
	"github.com/dgraph-io/badger/v4"
	"github.com/je4/ubcat/v2/pkg/schema"
	"github.com/je4/utils/v2/pkg/openai"
	"github.com/je4/utils/v2/pkg/zLogger"
	oai "github.com/sashabaranov/go-openai"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/marc21"
	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
	"text/template"
)

func NewEmbedding(tpl *template.Template, flags []string, badgerDB *badger.DB, openaiApiKey string, logger zLogger.ZLogger) *Embedding {
	kvBadger := openai.NewKVBadger(badgerDB)
	client := openai.NewClientV2(openaiApiKey, kvBadger, logger)
	emb := &Embedding{
		tpl:    tpl,
		flags:  flags,
		client: client,
		logger: logger,
	}

	return emb
}

type Embedding struct {
	tpl    *template.Template
	flags  []string
	client *openai.ClientV2
	logger zLogger.ZLogger
}

func (emb *Embedding) GetEmbedding(marc *marc21.Record) (embeddings map[string][]float32, resultErr error) {
	jsonBytes, err := json.Marshal(marc)
	if err != nil {
		resultErr = errors.Wrap(err, "cannot marshal marc21 record")
		return
	}
	entry := &schema.UBSchema{}
	if err := json.Unmarshal(jsonBytes, entry); err != nil {
		resultErr = errors.Wrap(err, "cannot unmarshal json")
		return
	}

	var buf = bytes.NewBuffer(nil)
	if err := emb.tpl.Execute(buf, entry); err != nil {
		err = errors.Wrap(err, "cannot execute template")
		resultErr = err
		return
	}

	embeddings = map[string][]float32{}
	//	str := buf.String()
	emb.logger.Debug().Msgf("embedding prose")
	e, err := emb.client.CreateEmbedding(buf.String(), oai.SmallEmbedding3)
	if err != nil {
		resultErr = errors.Wrap(err, "cannot create embedding")
		return
	}
	embeddings["prose"] = e.Embedding
	var marcStr string
	marcStr += fmt.Sprintf("LDR%s\n", marc.Leader.Text)
	for _, cf := range marc.Controlfields {
		marcStr += fmt.Sprintf("%s %s\n", cf.Tag, cf.Text)
	}
	keys := maps.Keys(marc.FieldLists)
	slices.Sort(keys)
	for _, fld := range keys {
		dfs := marc.FieldLists[fld]
		if slices.Contains([]string{"852", "866", "949", "019", "035", "040", "856", "900", "906", "910", "911", "912", "920", "930", "990", "991", "992", "993", "994", "995", "996", "997", "998", "999"}, fld) {
			continue
		}
		for _, df := range dfs {
			marcStr += fmt.Sprintf("%s%s\n", fld, df)
		}
	}
	emb.logger.Debug().Msg("embedding marc")
	e, err = emb.client.CreateEmbedding(marcStr, oai.SmallEmbedding3)
	if err != nil {
		err = errors.Wrap(err, "cannot create embedding for marc string")
		resultErr = err
		return
	}
	embeddings["marc"] = e.Embedding

	emb.logger.Debug().Msgf("embedding json")
	e, err = emb.client.CreateEmbedding(entry.GetAIJSON(), oai.SmallEmbedding3)
	if err != nil {
		resultErr = errors.Wrap(err, "cannot create embedding")
		return
	}
	embeddings["json"] = e.Embedding

	return embeddings, nil
}
