package gconfig

import (
	"context"
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/marc21"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"golang.org/x/oauth2/jwt"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
	"regexp"
	"sort"
	"strings"
	"time"
)

const LastSyncSheetName = "LastSync"

type GSheet struct {
	service *sheets.Service
}

func NewGSheet(auth *jwt.Config) (*GSheet, error) {
	client := auth.Client(context.TODO())
	srv, err := sheets.NewService(context.Background(), option.WithHTTPClient(client))
	if err != nil {
		return nil, errors.Wrap(err, "cannot connect to google sheets")
	}
	gsheet := &GSheet{service: srv}
	return gsheet, nil
}

func (gs *GSheet) LoadFlagConfig(mapper *marc21.Mapper, table, sheet string) error {
	readRange := fmt.Sprintf("%s!A2:E", sheet)
	resp, err := gs.service.Spreadsheets.Values.Get(table, readRange).Do()
	if err != nil {
		return errors.Wrapf(err, "cannot read from sheet %s", table)
	}
	for line, row := range resp.Values {
		var col = make([]string, 5)
		var rLen = len(row)
		for i := 0; i < 5; i++ {
			if rLen > i {
				col[i] = strings.TrimSpace(row[i].(string))
			}
		}
		if col[0] == "" {
			continue
		}
		flags := []string{}
		for _, flag := range strings.Split(col[0], ";") {
			flags = append(flags, strings.TrimSpace(flag))
		}
		fld := &marc21.Field{
			Flags:             flags,
			MarcField:         nil,
			Target:            col[2],
			Regexp:            nil,
			QualifierSubfield: "",
			QRegexp:           nil,
			Type:              "",
			TypeFormat:        "",
		}
		mf := strings.TrimSpace(col[1])
		if len(mf) == 6 && mf[0] != '^' {
			mf = "^" + mf + "$"
		}
		fld.MarcField, err = regexp.Compile(mf)
		if err != nil {
			return errors.Wrapf(err, "invalid regexp %s in %s%v", col[1], "%s!A", sheet, line+2)
		}
		rfld := strings.TrimSpace(col[2])
		if rfld != "" {
			fld.Regexp, err = regexp.Compile(rfld)
			if err != nil {
				return errors.Wrapf(err, "invalid regexp %s in %s!%s%v", rfld, sheet, "C", line+2)
			}
		}
		fld.QualifierSubfield = strings.TrimSpace(col[3])
		if fld.QualifierSubfield != "" {
			/*
				fld.QualifierField, err = regexp.Compile(qfld)
				if err != nil {
					return errors.Wrapf(err, "cannot compile qualifier field '%s' in %s!%s%v", qfld, sheet, "F", line+2)
				}
			*/
			fld.QRegexp, err = regexp.Compile(strings.TrimSpace(col[4]))
			if err != nil {
				return errors.Wrapf(err, "cannot compile qualifier regexp '%s' for subfield '%s' in %s!%s%v", strings.TrimSpace(col[6]), fld.QualifierSubfield, sheet, "G", line+2)
			}
		}
		mapper.MapperFields = append(mapper.MapperFields, fld)
	}
	sort.Slice(mapper.MapperFields, func(i, j int) bool {
		return len(mapper.MapperFields[i].MarcField.String()) < len(mapper.MapperFields[j].MarcField.String())
	})
	return nil
}

func (gs *GSheet) LoadMapperConfig(mapper *marc21.Mapper, table, sheet string) error {
	readRange := fmt.Sprintf("%s!A2:H", sheet)
	resp, err := gs.service.Spreadsheets.Values.Get(table, readRange).Do()
	if err != nil {
		return errors.Wrapf(err, "cannot read from sheet %s", table)
	}
	dateRegexp := regexp.MustCompile("(?i)^(date|datetime)\\(([^)]+)\\)$")
	for line, row := range resp.Values {
		var col = make([]string, 8)
		var rLen = len(row)
		for i := 0; i < 8; i++ {
			if rLen > i {
				col[i] = strings.TrimSpace(row[i].(string))
			}
		}
		if col[0] == "" {
			continue
		}
		flags := []string{}
		for _, flag := range strings.Split(col[0], ";") {
			flags = append(flags, strings.TrimSpace(flag))
		}
		fld := &marc21.Field{
			Flags:             flags,
			MarcField:         nil,
			Target:            col[2],
			Regexp:            nil,
			QualifierSubfield: "",
			QRegexp:           nil,
			Type:              "",
			TypeFormat:        "",
		}
		mf := strings.TrimSpace(col[1])
		if len(mf) == 6 && mf[0] != '^' {
			mf = "^" + mf + "$"
		}
		fld.MarcField, err = regexp.Compile(mf)
		if err != nil {
			return errors.Wrapf(err, "invalid regexp %s in %s%v", col[1], "%s!A", sheet, line+2)
		}
		rfld := strings.TrimSpace(col[3])
		if rfld != "" {
			fld.Regexp, err = regexp.Compile(rfld)
			if err != nil {
				return errors.Wrapf(err, "invalid regexp %s in %s!%s%v", rfld, sheet, "C", line+2)
			}
		}
		fld.Replace = strings.TrimSpace(col[4])
		t := strings.TrimSpace(col[7])
		matches := dateRegexp.FindStringSubmatch(t)
		if matches != nil {
			fld.Type = strings.ToLower(matches[1])
			fld.TypeFormat = matches[2]
		} else {
			fld.Type = t
		}
		fld.QualifierSubfield = strings.TrimSpace(col[5])
		if fld.QualifierSubfield != "" {
			/*
				fld.QualifierField, err = regexp.Compile(qfld)
				if err != nil {
					return errors.Wrapf(err, "cannot compile qualifier field '%s' in %s!%s%v", qfld, sheet, "F", line+2)
				}
			*/
			fld.QRegexp, err = regexp.Compile(strings.TrimSpace(col[6]))
			if err != nil {
				return errors.Wrapf(err, "cannot compile qualifier regexp '%s' for subfield '%s' in %s!%s%v", strings.TrimSpace(col[6]), fld.QualifierSubfield, sheet, "G", line+2)
			}
		}
		mapper.MapperFields = append(mapper.MapperFields, fld)
	}
	sort.Slice(mapper.MapperFields, func(i, j int) bool {
		return len(mapper.MapperFields[i].MarcField.String()) < len(mapper.MapperFields[j].MarcField.String())
	})
	return nil
}

func (gs *GSheet) LoadJSConfig(jsTable map[string]string, table, sheet string) error {
	readRange := fmt.Sprintf("%s!A2:B", sheet)
	resp, err := gs.service.Spreadsheets.Values.Get(table, readRange).Do()
	if err != nil {
		return errors.Wrapf(err, "cannot read from sheet %s", table)
	}
	for _, row := range resp.Values {
		var col = make([]string, 2)
		var rLen = len(row)
		for i := 0; i < 2; i++ {
			if rLen > i {
				col[i] = strings.TrimSpace(row[i].(string))
			}
		}
		if col[0] == "" {
			continue
		}
		jsTable[col[0]] = col[1]
	}
	return nil
}

func (gs *GSheet) LoadServiceConfig(serviceTable map[string]marc21.ServiceConfig, table, sheet string) error {
	readRange := fmt.Sprintf("%s!A2:C", sheet)
	resp, err := gs.service.Spreadsheets.Values.Get(table, readRange).Do()
	if err != nil {
		return errors.Wrapf(err, "cannot read from sheet %s", table)
	}
	for _, row := range resp.Values {
		var col = make([]string, 3)
		var rLen = len(row)
		for i := 0; i < 3; i++ {
			if rLen > i {
				col[i] = strings.TrimSpace(row[i].(string))
			}
		}
		if col[0] == "" {
			continue
		}
		serviceTable[col[0]] = marc21.ServiceConfig{
			UrlStr:      col[1],
			ServiceType: strings.ToLower(col[2]),
		}
	}
	return nil
}

func (gs *GSheet) LoadLastSyncValue(googleSheetId, syncEntryName string, logger *ublogger.Logger) (string, error) {
	readRange := fmt.Sprintf("%s!A2:B", LastSyncSheetName)
	for {
		resp, err := gs.service.Spreadsheets.Values.Get(googleSheetId, readRange).Do()
		if err != nil {
			if strings.Contains(err.Error(), "429") {
				logger.Info().Msgf("Exceeded read quota for Google Sheets API. Waiting for 1 minute.")
				time.Sleep(1 * time.Minute)
			} else {
				return "", errors.Wrapf(err, "cannot read from sheet %s", googleSheetId)
			}
		} else {
			for _, row := range resp.Values {
				if row[0] == syncEntryName {
					return row[1].(string), nil
				}
			}
			return "", errors.Errorf("no sync entry '%s' found for this instance in sheet %s", syncEntryName, googleSheetId)
		}
	}
}

func (gs *GSheet) SetLastSyncValue(googleSheetID string, syncEntryName string, value string, logger *ublogger.Logger) error {
	readRange := fmt.Sprintf("%s!A2:B", LastSyncSheetName)
	var err error
	var responseValueRange *sheets.ValueRange
	for {
		responseValueRange, err = gs.service.Spreadsheets.Values.Get(googleSheetID, readRange).Do()
		if err != nil {
			if strings.Contains(err.Error(), "429") {
				logger.Info().Msgf("Exceeded read quota for Google Sheets API. Waiting for 1 minute.")
				time.Sleep(1 * time.Minute)
			} else {
				return errors.Wrapf(err, "cannot read from sheet %s", googleSheetID)
			}
		} else {
			break
		}
	}
	var found = false
	for i, row := range responseValueRange.Values {
		if row[0] == syncEntryName {
			responseValueRange.Values[i][1] = value
			found = true
			break
		}
	}
	if !found {
		responseValueRange.Values = append(responseValueRange.Values, []interface{}{syncEntryName, value})
	}
	writeRange := fmt.Sprintf("%s!A2:B", LastSyncSheetName)
	for {
		_, err = gs.service.Spreadsheets.Values.Update(googleSheetID, writeRange, &sheets.ValueRange{Values: responseValueRange.Values}).ValueInputOption("RAW").Do()
		if err != nil {
			if strings.Contains(err.Error(), "429") {
				logger.Info().Msgf("Exceeded read quota for Google Sheets API. Waiting for 1 minute.")
				time.Sleep(1 * time.Minute)
			}
			return errors.Wrapf(err, "cannot write to sheet %s", googleSheetID)
		} else {
			return nil
		}
	}
}
