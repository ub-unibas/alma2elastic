// Copyright 2017-2018 Gregory Siems. All rights reserved.
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

// Package marc21 reads MARC21 and MARCXML data.
//
// Enables converting between MARC21 and MARCXML data.
package marc21

import (
	"emperror.dev/errors"
	"encoding/json"
	"fmt"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/marc21schema"
	"golang.org/x/exp/slices"
	"golang.org/x/text/unicode/norm"
	"log"
	"regexp"
	"strings"
	"time"
)

/*
   MARC-8 vs. UTF-8 encoding
       * leader.CharacterCodingScheme == "a" is UCS/Unicode
       * https://www.loc.gov/marc/specifications/speccharucs.html
       * https://www.loc.gov/marc/specifications/codetables.xml
       * https://lcweb2.loc.gov/diglib/codetables/eacc2uni.txt
*/

/*
https://www.loc.gov/marc/specifications/specrecstruc.html
*/

const (
	delimiter                  = byte(0x1f)
	fieldTerminator            = byte(0x1e)
	recordTerminator           = byte(0x1d)
	leaderLen                  = 24
	maxRecordSize              = 99999
	leaderPatternDsv4Ever      = `^[0-9]{6}[a-z][0-9u |\\-]{8}[a-z\\-].{19}([a-z|]{3})?`
	leaderPatternAlmaToElastic = `^[0-9]{6}[a-z][0-9u |]{8}[a-z].{19}[a-z|]{3}`
)

// Leader is for containing the text string of the MARC record Leader
type Leader struct {
	Text string `xml:",chardata" json:"text"`
}

func (ldr Leader) MarshalJSON() ([]byte, error) {
	if len(ldr.Text) != 24 {
		return nil, errors.Errorf("controlfield to short - '%s'", ldr.Text)
	}
	resultMap := map[string]string{}
	resultMap["leader_full"] = ldr.Text
	resultMap["leader_06_typeOfRecord"] = ldr.Text[6:7]
	resultMap["leader_07_bibliographicStatus"] = ldr.Text[7:8]
	resultMap["leader_17_encodingLevel"] = ldr.Text[17:18]
	resultMap["leader_19_multipartLevel"] = ldr.Text[19:20]
	return json.Marshal(resultMap)
}

func (ldr Leader) xMarshalJSON() ([]byte, error) {
	schema := marc21schema.GetSchema()
	ldrField := schema.Fields["LDR"]
	fields := ldrField.Positions.ToSolr(ldr.GetText())
	return json.Marshal(fields)
}

func (ldr Leader) ToSolr() map[string][]any {
	schema := marc21schema.GetSchema()
	ldrField := schema.Fields["LDR"]
	return ldrField.Positions.ToSolr(ldr.GetText())
}

type Date time.Time

func (d *Date) MarshalJSON() ([]byte, error) {
	str := (*time.Time)(d).Format("20060102")
	return json.Marshal(str)
}

type DateTime time.Time

func (d *DateTime) MarshalJSON() ([]byte, error) {
	str := (*time.Time)(d).Format("2006-01-02 15:04:05")
	return json.Marshal(str)
}

type FieldType struct {
	Keyword   []string    `json:"keyword,omitempty"`
	Date      []Date      `json:"date,omitempty"`
	DateTime  []DateTime  `json:"datetime,omitempty"`
	Long      []int64     `json:"long,omitempty"`
	Double    []float64   `json:"double,omitempty"`
	Object    []any       `json:"object,omitempty"`
	GeoShape  []*GeoShape `json:"location,omitempty"`
	Agent     []*Agent    `json:"agent,omitempty"`
	Concept   []*Concept  `json:"concept,omitempty"`
	Hierarchy []Hierarchy `json:"hierarchy,omitempty"`
	DateRange []DateRange `json:"daterange,omitempty"`
}

type FieldTypeNoStruct FieldType

func (ftns *FieldTypeNoStruct) MarshalJSON() ([]byte, error) {
	if len(ftns.Keyword) > 0 {
		return json.Marshal(ftns.Keyword)
	}
	if len(ftns.Date) > 0 {
		return json.Marshal(ftns.Date)
	}
	if len(ftns.DateTime) > 0 {
		return json.Marshal(ftns.DateTime)
	}
	if len(ftns.Long) > 0 {
		return json.Marshal(ftns.Long)
	}
	if len(ftns.Double) > 0 {
		return json.Marshal(ftns.Double)
	}
	if len(ftns.Object) > 0 {
		return json.Marshal(ftns.Object)
	}
	return json.Marshal(nil)
}

func (ftns *FieldTypeNoStruct) IsEmpty() bool {
	var totalLen int
	totalLen += len(ftns.Keyword)
	totalLen += len(ftns.Date)
	totalLen += len(ftns.DateTime)
	totalLen += len(ftns.Long)
	totalLen += len(ftns.Double)
	totalLen += len(ftns.Object)
	totalLen += len(ftns.Agent)
	totalLen += len(ftns.GeoShape)
	totalLen += len(ftns.Concept)
	totalLen += len(ftns.Hierarchy)
	totalLen += len(ftns.DateRange)
	return totalLen == 0
}

type Facet struct {
	Name string `json:"name"`
	*FieldType
}

type ResultFacet struct {
	String    []*FacetString    `json:"strings,omitempty"`
	Date      []*FacetDate      `json:"dates,omitempty"`
	Datetime  []*FacetDateTime  `json:"datetimes,omitempty"`
	Long      []*FacetLong      `json:"longs,omitempty"`
	Double    []*FacetDouble    `json:"doubles,omitempty"`
	Agent     []*FacetAgent     `json:"agents,omitempty"`
	GeoShape  []*FacetGeoShape  `json:"geoshapes,omitempty"`
	Object    []*FacetObject    `json:"objects,omitempty"`
	Concept   []*FacetConcept   `json:"concepts,omitempty"`
	Hierarchy []*FacetHierarchy `json:"hierarchy,omitempty"`
	DateRange []*FacetDateRange `json:"daterange,omitempty"`
}

func (f *ResultFacet) clean() {
	for _, facet := range f.String {
		//		slices.Sort(facet.Values)
		facet.Values = slices.Compact(facet.Values)
	}
	for _, facet := range f.Long {
		//		slices.Sort(facet.Values)
		facet.Values = slices.Compact(facet.Values)
	}
	for _, facet := range f.Double {
		//		slices.Sort(facet.Values)
		facet.Values = slices.Compact(facet.Values)
	}
	for _, facet := range f.Date {
		/*		slices.SortFunc(facet.Values, func(a, b Date) bool {
					return time.Time(a).Before(time.Time(b))
				})
		*/
		facet.Values = slices.Compact(facet.Values)
	}
	for _, facet := range f.Datetime {
		/*		slices.SortFunc(facet.Values, func(a, b DateTime) bool {
					return time.Time(a).Before(time.Time(b))
				})
		*/
		facet.Values = slices.Compact(facet.Values)
	}
	/*
		for _, facet := range f.Agent {
			slices.SortFunc(facet.Values, func(a, b Agent) bool {
				return a.Name < b.Name
			})
			facet.Values = slices.Compact(facet.Values)
		}

	*/

}

func (f *ResultFacet) addString(name string, keyword []string) {
	for _, fs := range f.String {
		if fs.Name == name {
			fs.Values = append(fs.Values, keyword...)
			return
		}
	}
	f.String = append(f.String, &FacetString{
		Name:   name,
		Values: keyword,
	})
}

func (f *ResultFacet) addHierarchy(name string, keyword []Hierarchy) {
	for _, fs := range f.Hierarchy {
		if fs.Name == name {
			fs.Values = append(fs.Values, keyword...)
			return
		}
	}
	f.Hierarchy = append(f.Hierarchy, &FacetHierarchy{
		Name:   name,
		Values: keyword,
	})
}

func (f *ResultFacet) addLong(name string, dates []int64) {
	for _, fs := range f.Long {
		if fs.Name == name {
			fs.Values = append(fs.Values, dates...)
			return
		}
	}
	f.Long = append(f.Long, &FacetLong{
		Name:   name,
		Values: dates,
	})
}

func (f *ResultFacet) addDouble(name string, dates []float64) {
	for _, fs := range f.Double {
		if fs.Name == name {
			fs.Values = append(fs.Values, dates...)
			return
		}
	}
	f.Double = append(f.Double, &FacetDouble{
		Name:   name,
		Values: dates,
	})
}

func (f *ResultFacet) addDate(name string, dates []Date) {
	for _, fs := range f.Date {
		if fs.Name == name {
			fs.Values = append(fs.Values, dates...)
			return
		}
	}
	f.Date = append(f.Date, &FacetDate{
		Name:   name,
		Values: dates,
	})
}

func (f *ResultFacet) addDateTime(name string, dates []DateTime) {
	for _, fs := range f.Datetime {
		if fs.Name == name {
			fs.Values = append(fs.Values, dates...)
			return
		}
	}
	f.Datetime = append(f.Datetime, &FacetDateTime{
		Name:   name,
		Values: dates,
	})
}

func hasKey(m map[string]any, key string) bool {
	key = strings.ToLower(key)
	for k, _ := range m {
		if strings.ToLower(k) == key {
			return true
		}
	}
	return false
}

func (f *ResultFacet) addObject(name string, objects []any) {
	for _, fs := range f.Object {
		if fs.Name == name {
			fs.Values = append(fs.Values, objects...)
			return
		}
	}
	f.Object = append(f.Object, &FacetObject{
		Name:   name,
		Values: objects,
	})
}

func (f *ResultFacet) addAgent(name string, agents []*Agent) {
	for _, fs := range f.Agent {
		if fs.Name == name {
			fs.Values = append(fs.Values, agents...)
			return
		}
	}
	f.Agent = append(f.Agent, &FacetAgent{
		Name:   name,
		Values: agents,
	})
}

func (f *ResultFacet) addConcept(name string, concepts []*Concept) {
	for _, fs := range f.Concept {
		if fs.Name == name {
			fs.Values = append(fs.Values, concepts...)
			return
		}
	}
	f.Concept = append(f.Concept, &FacetConcept{
		Name:   name,
		Values: concepts,
	})
}

func (f *ResultFacet) addGeoShape(name string, shapes []*GeoShape) {
	for _, fs := range f.GeoShape {
		if fs.Name == name {
			fs.Values = append(fs.Values, shapes...)
			return
		}
	}
	f.GeoShape = append(f.GeoShape, &FacetGeoShape{
		Name:   name,
		Values: shapes,
	})
}

func (f *ResultFacet) addDateRange(name string, ranges []DateRange) {
	for _, fs := range f.DateRange {
		if fs.Name == name {
			fs.Values = append(fs.Values, ranges...)
			return
		}
	}
	f.DateRange = append(f.DateRange, &FacetDateRange{
		Name:   name,
		Values: ranges,
	})
}

func (f *ResultFacet) add(facet *Facet) {
	switch {
	case len(facet.Keyword) > 0:
		f.addString(facet.Name, facet.Keyword)
	case len(facet.Date) > 0:
		f.addDate(facet.Name, facet.Date)
	case len(facet.DateTime) > 0:
		f.addDateTime(facet.Name, facet.DateTime)
	case len(facet.Long) > 0:
		f.addLong(facet.Name, facet.Long)
	case len(facet.Double) > 0:
		f.addDouble(facet.Name, facet.Double)
	case len(facet.Object) > 0:
		f.addObject(facet.Name, facet.Object)
	case len(facet.GeoShape) > 0:
		f.addGeoShape(facet.Name, facet.GeoShape)
	case len(facet.Agent) > 0:
		f.addAgent(facet.Name, facet.Agent)
	case len(facet.Concept) > 0:
		f.addConcept(facet.Name, facet.Concept)
	case len(facet.Hierarchy) > 0:
		f.addHierarchy(facet.Name, facet.Hierarchy)
	case len(facet.DateRange) > 0:
		f.addDateRange(facet.Name, facet.DateRange)
	}

}

type FacetString struct {
	Name   string   `json:"name"`
	Values []string `json:"string"`
}

type FacetHierarchy struct {
	Name   string      `json:"name"`
	Values []Hierarchy `json:"hierarchy"`
}

type FacetDateRange struct {
	Name   string      `json:"name"`
	Values []DateRange `json:"daterange"`
}

type FacetDate struct {
	Name   string `json:"name"`
	Values []Date `json:"date"`
}

type FacetDateTime struct {
	Name   string     `json:"name"`
	Values []DateTime `json:"datetime"`
}

type FacetLong struct {
	Name   string  `json:"name"`
	Values []int64 `json:"long"`
}

type FacetDouble struct {
	Name   string    `json:"name"`
	Values []float64 `json:"double"`
}

type Hierarchy string

type Concept struct {
	Label      string   `json:"label"`
	Identifier []string `json:"identifier"`
}

type DateRange struct {
	GTE string `json:"gte,omitempty"`
	LTE string `json:"lte,omitempty"`
}

type Agent struct {
	Concept
	Role []string `json:"role,omitempty"`
}

type FacetConcept struct {
	Name   string     `json:"name"`
	Values []*Concept `json:"concept"`
}

type FacetAgent struct {
	Name   string   `json:"name"`
	Values []*Agent `json:"agent"`
}

type GeoShape struct {
	Name       []string `json:"name,omitempty"`
	Identifier []string `json:"identifier,omitempty"`
	Location   []any    `json:"location"`
}

type FacetGeoShape struct {
	Name   string      `json:"name"`
	Values []*GeoShape `json:"shapes"`
}

type FacetObject struct {
	Name   string `json:"name"`
	Values []any  `json:"objects"`
}

type Controlfields []*Controlfield

func (cfs Controlfields) MarshalJSON() ([]byte, error) {
	result := map[string]string{}
	for _, cf := range cfs {
		l := len(cf.Text)
		switch cf.Tag {
		case "001":
			//result["001_id"] = cf.Text
		case "003":
			//result["003_controlNumberId"] = cf.Text
		case "005":
			//result["005_latestTransactionTime"] = cf.Text
		case "006":
			//			result["006_full"] = cf.Text
		case "007":
			//			result["007_full"] = cf.Te	xt
		case "008":
			if l != 40 {
				matched, _ := regexp.MatchString(leaderPatternAlmaToElastic, cf.Text)
				matched, _ = regexp.MatchString(leaderPatternDsv4Ever, cf.Text)
				if matched != true {
					return nil, errors.Errorf("controlfield invalid - '%s'", cf.Text)
				}
			}
			//			result["008_full"] = cf.Text
			result["008_06_typeOfDate"] = cf.Text[6:7]
			result["008_07-10_dateFirst"] = cf.Text[7:11]
			result["008_11-14_dateSecond"] = cf.Text[11:15]
			result["008_15-17_country"] = cf.Text[15:18]
			if len(cf.Text) >= 38 {
				result["008_35-37_language"] = cf.Text[35:38]
			}
			switch cf.Type {
			case "Books", "Computer Files", "Music", "Continuing Resources", "Mixed Materials":
				result["008_23or29_formOfItem"] = cf.Text[23:24]
			case "Visual Materials", "Maps":
				result["008_23or29_formOfItem"] = cf.Text[29:30]
			default:
			}
		default:
		}
		result[cf.Tag] = cf.Text
	}
	return json.Marshal(result)
}

func (cfs Controlfields) xMarshalJSON() ([]byte, error) {
	result := map[string]any{}
	for _, cf := range cfs {
		fields := cf.buildFields()
		for k, v := range fields {
			result[k] = v
		}
	}
	return json.Marshal(result)
}

type Mapping map[string]*FieldTypeNoStruct

func buildHier(val *FieldTypeNoStruct, base map[string]any, hier []string) map[string]any {
	if len(hier) < 1 {
		return nil
	}
	n := hier[0]

	if _, ok := base[n]; !ok {
		if len(hier) == 1 {
			base[n] = val
		} else {
			base[n] = buildHier(val, map[string]any{}, hier[1:])
		}
	} else {
		if len(hier) == 1 {
			ft, okType := base[n].(*FieldTypeNoStruct)
			if !okType {
				log.Fatalf("invalid type for hierarchy %v: %v", hier, base[n])
			}
			ft.Keyword = append(ft.Keyword, val.Keyword...)
			ft.Date = append(ft.Date, val.Date...)
			ft.DateTime = append(ft.DateTime, val.DateTime...)
			ft.Long = append(ft.Long, val.Long...)
			ft.Double = append(ft.Double, val.Double...)
			ft.Object = append(ft.Object, val.Object...)
		} else {
			base[n] = buildHier(val, base[n].(map[string]any), hier[1:])
		}
	}
	return base
}

func (m Mapping) MarshalJSON() ([]byte, error) {
	base := map[string]any{}
	for k, v := range m {
		parts := strings.Split(k, ".")
		base = buildHier(v, base, parts)
	}
	return json.Marshal(base)
}

// Record is for containing a MARC record
type Record struct {
	Timestamp      string              `xml:"-" json:"timestamp"`
	OAIID          []string            `xml:"-" json:"oai_id,omitempty"`
	Leader         Leader              `xml:"leader" json:"LDR"`
	Controlfields  Controlfields       `xml:"controlfield" json:"controlfield"`
	Datafields     []*Datafield        `xml:"datafield" json:"datafield"`
	FieldLists     map[string][]string `xml:"-" json:"fieldlists,omitempty"`
	Mapping        Mapping             `xml:"-" json:"mapping,omitempty"`
	Facet          *ResultFacet        `xml:"-" json:"facet,omitempty"`
	Additional     map[string][]any    `xml:"-" json:"additional,omitempty"`
	Sets           []string            `xml:"-" json:"sets,omitempty"`
	Flags          []string            `xml:"-" json:"flags,omitempty"`
	EmbeddingProse []float32           `json:"embedding_prose,omitempty"`
	EmbeddingMarc  []float32           `json:"embedding_marc,omitempty"`
	EmbeddingJSON  []float32           `json:"embedding_json,omitempty"`

	ACL    map[string][]string `xml:"-" json:"acl,omitempty"`
	fields map[string][]string
}

func (rec *Record) Optimize(sets []string) error {
	if rec.Datafields == nil {
		return errors.Errorf("record has no datafields")
	}
	rec.Mapping = map[string]*FieldTypeNoStruct{}
	rec.Sets = sets
	ldr := rec.Leader.GetText()
	if len(ldr) < 8 {
		return errors.Errorf("leader to short - '%s'", ldr)
	}
	l6 := ldr[6:7]
	l7 := ldr[7:8]
	var t string
	switch {
	case strings.Contains("at", l6) && strings.Contains("acdm", l7):
		t = "Books"
	case strings.Contains("at", l6) && strings.Contains("bsi", l7):
		t = "Continuing Resources"
	case strings.Contains("cdij", l6):
		t = "Music"
	case strings.Contains("ef", l6):
		t = "Maps"
	case strings.Contains("gkor", l6):
		t = "Visual Materials"
	case l6 == "m":
		t = "Computer Files"
	case strings.Contains("bp", l6):
		t = "Mixed Materials"
	}
	for k, _ := range rec.Controlfields {
		rec.Controlfields[k].Type = t
	}
	if rec.FieldLists == nil {
		rec.FieldLists = map[string][]string{}
	}

	for k, val := range rec.Datafields {
		if strings.HasPrefix(val.Tag, "control") {
			continue
		}
		for _, sub := range val.Subfields {
			sub.Decompose()
		}
		if _, ok := rec.FieldLists[val.Tag]; !ok {
			rec.FieldLists[val.Tag] = []string{}
		}
		str := fmt.Sprintf("%s%s", strings.Replace(val.Ind1, string(' '), string('#'), -1), strings.Replace(val.Ind2, string(' '), string('#'), -1))
		for l, sub := range rec.Datafields[k].Subfields {
			rec.Datafields[k].Subfields[l].Datafield = rec.Datafields[k]
			str += fmt.Sprintf("$$%s%s", sub.Code, sub.Text)
		}
		str += "|"
		rec.FieldLists[val.Tag] = append(rec.FieldLists[val.Tag], str)
	}

	/*
		for fld, _ := range rec.FieldLists {
			slices.Sort(rec.FieldLists[fld])
			rec.FieldLists[fld] = slices.Compact(rec.FieldLists[fld])
		}
	*/
	return nil
}

func (rec *Record) ToSolr() (result map[string][]any) {
	result = map[string][]any{}
	r := rec.Leader.ToSolr()
	for k, vs := range r {
		if _, ok := result[k]; !ok {
			result[k] = []any{}
		}
		result[k] = append(result[k], vs...)
	}
	for _, df := range rec.Datafields {
		for k, vs := range df.ToSolr() {
			if _, ok := result[k]; !ok {
				result[k] = []any{}
			}
			result[k] = append(result[k], vs...)
		}
	}
	return
}

func (rec *Record) Map(fieldMapper, facetMapper, flagMapper, aclMapper *Mapper) error {
	rec.Timestamp = time.Now().Format("2006-01-02T15:04:05Z")
	rec.Facet = &ResultFacet{
		String:    []*FacetString{},
		Date:      []*FacetDate{},
		Datetime:  []*FacetDateTime{},
		Long:      []*FacetLong{},
		Double:    []*FacetDouble{},
		Agent:     []*FacetAgent{},
		GeoShape:  []*FacetGeoShape{},
		Object:    []*FacetObject{},
		Concept:   []*FacetConcept{},
		Hierarchy: []*FacetHierarchy{},
		DateRange: []*FacetDateRange{},
	}

	dataFields := []*Datafield{}
	for _, fld := range rec.Datafields {
		dataFields = append(dataFields, fld)
	}
	for _, val := range rec.Controlfields {
		fld := &Datafield{
			Tag:       "control" + val.Tag,
			Ind1:      " ",
			Ind2:      " ",
			Subfields: nil,
		}
		fld.Subfields = []*Subfield{
			&Subfield{
				Code:      " ",
				Text:      val.Text,
				Datafield: fld,
			},
		}

		dataFields = append(dataFields, fld)
	}

	fld := &Datafield{
		Tag:       "leader",
		Ind1:      " ",
		Ind2:      " ",
		Subfields: nil,
	}
	fld.Subfields = []*Subfield{
		&Subfield{
			Code:      " ",
			Text:      rec.Leader.GetText(),
			Datafield: fld,
		},
	}
	dataFields = append(dataFields, fld)

	rec.ACL = map[string][]string{}

	flags := []string{"all"}
	for _, fld := range dataFields {
		for _, sub := range fld.Subfields {
			subFlags, err := flagMapper.MapFlags(sub)
			if err != nil {
				return errors.Wrapf(err, "cannot map subfield %s", sub.GetCode())
			}
			for _, flag := range subFlags {
				if !slices.Contains(flags, flag) {
					flags = append(flags, flag)
				}
			}
		}
	}
	rec.Flags = flags
	for _, fld := range dataFields {
		for _, sub := range fld.Subfields {
			//
			// Mapping
			//

			mapping, err := fieldMapper.Map(sub, flags, rec)
			if err != nil {
				return errors.Wrapf(err, "cannot map subfield %s", sub.GetCode())
			}

			for k, v := range mapping {
				if v.IsEmpty() {
					continue
				}
				if _, ok := rec.Mapping[k]; !ok {
					rec.Mapping[k] = &FieldTypeNoStruct{
						Keyword:   []string{},
						Date:      []Date{},
						DateTime:  []DateTime{},
						Long:      []int64{},
						Double:    []float64{},
						Object:    []any{},
						GeoShape:  []*GeoShape{},
						Agent:     []*Agent{},
						Hierarchy: []Hierarchy{},
						Concept:   []*Concept{},
						DateRange: []DateRange{},
					}
				}
				rec.Mapping[k].Keyword = append(rec.Mapping[k].Keyword, v.Keyword...)
				rec.Mapping[k].Date = append(rec.Mapping[k].Date, v.Date...)
				rec.Mapping[k].DateTime = append(rec.Mapping[k].DateTime, v.DateTime...)
				rec.Mapping[k].Long = append(rec.Mapping[k].Long, v.Long...)
				rec.Mapping[k].Double = append(rec.Mapping[k].Double, v.Double...)
				rec.Mapping[k].Object = appendObjectIfNew(rec.Mapping[k].Object, v.Object...)
				rec.Mapping[k].Agent = append(rec.Mapping[k].Agent, v.Agent...)
				rec.Mapping[k].Concept = append(rec.Mapping[k].Concept, v.Concept...)
				rec.Mapping[k].Hierarchy = append(rec.Mapping[k].Hierarchy, v.Hierarchy...)
				rec.Mapping[k].GeoShape = append(rec.Mapping[k].GeoShape, v.GeoShape...)
				rec.Mapping[k].DateRange = append(rec.Mapping[k].DateRange, v.DateRange...)
			}

			for k, v := range mapping {
				if v.IsEmpty() {
					continue
				}
				//slices.Sort(rec.Mapping[k].Keyword)
				rec.Mapping[k].Keyword = slices.Compact(rec.Mapping[k].Keyword)
				/*slices.SortFunc(rec.Mapping[k].Date, func(a, b Date) bool {
					return time.Time(a).Before(time.Time(b))
				})
				*/
				rec.Mapping[k].Date = slices.Compact(rec.Mapping[k].Date)
				/*slices.SortFunc(rec.Mapping[k].DateTime, func(a, b DateTime) bool {
					return time.Time(a).Before(time.Time(b))
				})
				*/
				rec.Mapping[k].DateTime = slices.Compact(rec.Mapping[k].DateTime)
				//slices.Sort(rec.Mapping[k].Long)
				rec.Mapping[k].Long = slices.Compact(rec.Mapping[k].Long)
				//slices.Sort(rec.Mapping[k].Double)
				rec.Mapping[k].Double = slices.Compact(rec.Mapping[k].Double)
				//rec.Mapping[k].Object = slices.Compact(rec.Mapping[k].Object)
			}

			//
			// Facets
			//
			facets, err := facetMapper.MapFacet(sub, flags, rec)
			if err != nil {
				return errors.Wrapf(err, "cannot map subfield %s", sub.GetCode())
			}
			if facets != nil {
				for _, facet := range facets {
					if facet != nil {
						rec.Facet.add(facet)
					} else {
						return errors.Errorf("facet is nil")
					}
				}
				rec.Facet.clean()
			} else {
				return errors.Errorf("facets are nil")
			}

			//
			// ACLs
			//
			ACLs, err := aclMapper.MapACL(sub, flags)
			if err != nil {
				return errors.Wrapf(err, "cannot map subfield %s", sub.GetCode())
			}
			if ACLs != nil {
				for k, groups := range ACLs {
					if groups != nil {
						if _, ok := rec.ACL[k]; ok {
							rec.ACL[k] = []string{}
						}
						rec.ACL[k] = append(rec.ACL[k], groups...)
					} else {
						return errors.Errorf("ACL is nil")
					}
				}
				// clean it up
				for k, groups := range rec.ACL {
					//slices.Sort(groups)
					rec.ACL[k] = slices.Compact(groups)
				}
			} else {
				return errors.Errorf("ACLs are nil")
			}
		}
	}
	return nil
}

// Controlfield contains a controlfield entry
type Controlfield struct {
	Tag  string `xml:"tag,attr" json:"tag"`
	Text string `xml:",chardata" json:"text"`
	Type string `xml:"-" json:"-"`
}

func (ctrl Controlfield) buildFields() map[string][]any {
	var fields map[string][]any
	schema := marc21schema.GetSchema()
	ctrlField := schema.Fields[ctrl.GetTag()]

	var fldname string
	if ctrlField != nil && ctrlField.Solr != "" {
		fldname = ctrlField.Solr
	} else {
		fldname = ctrl.GetTag()
	}
	fields = map[string][]any{fldname: []any{ctrl.GetText()}}

	if ctrlField != nil {
		if ctrlField.Positions == nil {
			if ctrlField.Types != nil && ctrl.Type != "" {
				if t, ok := ctrlField.Types["All Materials"]; ok {
					if p := t.Positions; p != nil {
						fields = p.ToSolr(ctrl.GetText())
					}
				}
				if t, ok := ctrlField.Types[ctrl.Type]; ok {
					if p := t.Positions; p != nil {
						for k, v := range p.ToSolr(ctrl.GetText()) {
							fields[k] = v
						}
					}
				}
			} else {
			}
		} else {
			fields = ctrlField.Positions.ToSolr(ctrl.GetText())
		}
	}
	return fields
}

func (ctrl Controlfield) MarshalJSON() ([]byte, error) {
	fields := ctrl.buildFields()
	return json.Marshal(fields)
}

// Datafield contains a datafield entry
type Datafield struct {
	Tag       string      `xml:"tag,attr" json:"tag"`
	Ind1      string      `xml:"ind1,attr" json:"ind1,omitempty"`
	Ind2      string      `xml:"ind2,attr" json:"ind2,omitempty"`
	Subfields []*Subfield `xml:"subfield" json:"subfield,omitempty"`
}

func (df Datafield) ToSolr() (result map[string][]any) {
	result = map[string][]any{}
	schema := marc21schema.GetSchema()
	fldSchema := schema.Fields[df.GetTag()]
	if df.Subfields != nil {
		for _, sf := range df.Subfields {
			var fldname = strings.ReplaceAll("unknown_"+df.GetTag()+df.GetInd1()+df.GetInd2()+sf.Code, " ", "_")
			if fldSchema != nil {
				sfSchema := fldSchema.Subfields[sf.GetCode()]
				if sfSchema != nil {
					fldname = strings.ReplaceAll(sfSchema.Solr[0:3]+df.GetInd1()+df.GetInd2()+sfSchema.Solr[3:], " ", "_")
				}
			}
			if _, ok := result[fldname]; !ok {
				result[fldname] = []any{}
			}
			result[fldname] = append(result[fldname], sf.GetText())
		}
	}
	return
}

// Subfield contains a subfield entry
type Subfield struct {
	Code      string `xml:"code,attr" json:"code"`
	Text      string `xml:",chardata" json:"text"`
	Datafield *Datafield
}

func (sub *Subfield) MarshalJSON() ([]byte, error) {
	return json.Marshal(fmt.Sprintf("|%s %s", sub.GetCode(), sub.GetText()))
	//var subMap = map[string]string{sub.GetCode(): sub.GetText()}
	//return json.Marshal(subMap)
}

var subfieldRegexp = regexp.MustCompile(`^\\|(.) (.+)$`)

func (sub *Subfield) UnmarshalJSON(data []byte) error {
	var str string
	if err := json.Unmarshal(data, &str); err != nil {
		return errors.WithStack(err)
	}
	matches := subfieldRegexp.FindStringSubmatch(str)
	if matches == nil {
		return errors.Errorf("invalid subfield content '%s'", string(data))
	}
	sub.Code = matches[1]
	sub.Text = matches[2]
	/*
		var subMap = map[string]string{}
		if err := json.Unmarshal(data, &subMap); err != nil {
			return errors.WithStack(err)
		}
		for key, val := range subMap {
			sub.Code = key
			sub.Text = val
			break
		}
	*/
	return nil
}

func (sub *Subfield) GetMarc() string {
	sb := strings.Builder{}
	sb.WriteString(sub.Datafield.GetTag())
	sb.WriteString(sub.Datafield.GetInd1())
	sb.WriteString(sub.Datafield.GetInd2())
	sb.WriteString(sub.GetCode())
	return sb.String()
}

func (sub *Subfield) Decompose() {
	sub.Text = norm.NFC.String(sub.Text)
}
