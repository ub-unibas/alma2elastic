package marc21

import "strings"

type Corporate struct {
	Name        string   `json:"namePart"`
	Description []string `json:"description,omitempty"`
	Role        []string `json:"role,omitempty"`
	Identifier  string   `json:"identifier,omitempty"`
}

type CorporateResult map[string][]Corporate

func corporate(target string, replace string, init *Datafield, rec *Record) ([]any, error) {
	var objects = map[string][]*Datafield{}
	for _, object := range rec.Datafields {
		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []CorporateResult{}
	var personFields = []*Datafield{}
	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}
	field710, ok := objects["710"]
	if ok {
		personFields = append(personFields, field710...)
	}
	field110, ok := objects["110"]
	if ok {
		personFields = append(personFields, field110...)
	}
	for _, field := range personFields {
		var result = CorporateResult{}
		var linkedField string
		var name string
		var description []string
		var role []string
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			var ok bool
			if field.Tag == "110" {
				ok = true
			}
			if field.Tag == "710" && field.Ind2 != "2" {
				ok = true
			}
			if ok {
				switch subField.Code {
				case "a":
					name = subField.Text
				case "b":
					name += ", " + subField.Text
				case "g":
					description = append(description, subField.Text)
				case "0":
					identifier = subField.Text
				case "4":
					role = append(role, subField.Text)
				case "6":
					linkedField = subField.Text
				}
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		if name != "" {
			if _, ok := result[authority]; !ok {
				result[authority] = []Corporate{}
			}
			result[authority] = append(result[authority], Corporate{
				Name:        name,
				Description: description,
				Role:        role,
				Identifier:  identifier,
			})
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var description []string
				var role []string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "110-") || strings.HasPrefix(subField.Text, "710-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						name = subField.Text
					case "b":
						name += ", " + subField.Text
					case "g":
						description = append(description, subField.Text)
					case "0":
						identifier = subField.Text
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if name != "" {
					if _, ok := result[authority]; !ok {
						result[authority] = []Corporate{}
					}
					result[authority] = append(result[authority], Corporate{
						Name:        name,
						Description: description,
						Role:        role,
						Identifier:  identifier,
					})
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
