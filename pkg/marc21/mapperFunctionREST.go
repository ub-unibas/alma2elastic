package marc21

import (
	"emperror.dev/errors"
	"github.com/rs/zerolog/log"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/js"
)

// MapperFunctionREST
// handles all scalar values for mapping
type MapperFunctionREST struct {
	re *RESTEngine
}

func NewMapperFunctionREST(re *RESTEngine) (*MapperFunctionREST, error) {
	return &MapperFunctionREST{re: re}, nil
}

func (m *MapperFunctionREST) GetExt() string {
	return "srv"
}

func (m *MapperFunctionREST) Run(mapperField *Field, field *Subfield, record *Record) (*FieldTypeNoStruct, error) {
	if m.re == nil {
		log.Debug().Msgf("No rest engine defined. Do not run.")
		return nil, nil
	}
	jsInit := &js.FieldInitializer{
		Tag:      field.Datafield.Tag,
		Ind1:     field.Datafield.Ind1,
		Ind2:     field.Datafield.Ind2,
		Subfield: map[string][]string{},
	}
	for _, sf := range field.Datafield.Subfields {
		if _, ok := jsInit.Subfield[sf.GetCode()]; !ok {
			jsInit.Subfield[sf.GetCode()] = []string{}
		}
		jsInit.Subfield[sf.GetCode()] = append(jsInit.Subfield[sf.GetCode()], sf.GetText())
	}

	object, err := m.re.RunField(mapperField.Target, mapperField.Replace, field.Datafield, record)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot run %s on field %s", mapperField.Replace, field.GetMarc())
	}

	result, err := getResult(object, mapperField.Type, mapperField.TypeFormat)
	return result, errors.WithStack(err)
}

var (
	_ MapperFunction = (*MapperFunctionREST)(nil)
)
