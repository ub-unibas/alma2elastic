package marc21

import (
	"emperror.dev/errors"
)

type internalFunction func(target string, replace string, init *Datafield, rec *Record) ([]any, error)

// MapperFunctionInt
// handles all scalar values for mapping
type MapperFunctionInt struct {
	functions map[string]internalFunction
}

func NewMapperFunctionInt() (*MapperFunctionInt, error) {
	return &MapperFunctionInt{functions: map[string]internalFunction{
		"person.int":     person,
		"corporate.int":  corporate,
		"conference.int": conference,
		"family.int":     family,
		"holding.int":    holding,
		"ddc.int":        ddc,
		"date.int":       date,
		"agent.int":      agent,
	}}, nil
}

func (m *MapperFunctionInt) GetExt() string {
	return "int"
}
func (m *MapperFunctionInt) Run(mapperField *Field, field *Subfield, record *Record) (*FieldTypeNoStruct, error) {
	var valstr = field.GetText()
	if mapperField.Regexp != nil {
		if !mapperField.Regexp.MatchString(valstr) {
			return nil, nil
		}
	}
	for name, f := range m.functions {
		if name == mapperField.Replace {
			result, err := f(mapperField.Target, mapperField.Replace, field.Datafield, record)
			resultVal := &FieldTypeNoStruct{
				Keyword:   []string{},
				Date:      []Date{},
				DateTime:  []DateTime{},
				Long:      []int64{},
				Double:    []float64{},
				Object:    []any{},
				GeoShape:  []*GeoShape{},
				Agent:     []*Agent{},
				DateRange: []DateRange{},
				Hierarchy: []Hierarchy{},
				Concept:   []*Concept{},
			}
			if result != nil {
				for _, r := range result {
					switch mapperField.Type {
					case "string":
						str, ok := r.(string)
						if !ok {
							return nil, errors.Errorf("invalid type '%T' in target field '%s'. should be string", r, field.GetMarc())
						}
						resultVal.Keyword = append(resultVal.Keyword, str)
					case "daterange":
						dr, ok := r.(DateResult)
						if !ok {
							return nil, errors.Errorf("invalid type '%T' in target field '%s'. should be DateResult", r, field.GetMarc())
						}
						resultVal.DateRange = append(resultVal.DateRange, DateRange{
							GTE: dr.Gte,
							LTE: dr.Lte,
						})
					default:
						resultVal.Object = append(resultVal.Object, r)
					}
				}

			}
			return resultVal, errors.WithStack(err)
		}
	}
	return nil, errors.Errorf("invalid function '%s' in target field '%s'", mapperField.Target, field.GetMarc())
}

var (
	_ MapperFunction = (*MapperFunctionInt)(nil)
)
