// Copyright 2017-2018 Gregory Siems. All rights reserved.
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package marc21

import (
	"fmt"
	"strings"
)

/*
https://www.loc.gov/marc/specifications/specrecstruc.html

    Control fields in MARC 21 formats are assigned tags beginning with
    two zeroes. They are comprised of data and a field terminator; they
    do not contain indicators or subfield codes. The control number
    field is assigned tag 001 and contains the control number of the
    record. Each record contains only one control number field (with
    tag 001), which is to be located at the base address of data.
*/

/*
http://www.loc.gov/marc/bibliographic/bdintro.html

    Variable control fields - The 00X fields. These fields are
    identified by a field tag in the Directory but they contain neither
    indicator positions nor subfield codes. The variable control fields
    are structurally different from the variable data fields. They may
    contain either a single data element or a series of fixed-length
    data elements identified by relative character position.
*/

// http://www.loc.gov/marc/bibliographic/bd00x.html
// http://www.loc.gov/marc/holdings/hd00x.html
// http://www.loc.gov/marc/authority/ad00x.html
// http://www.loc.gov/marc/classification/cd00x.html
// http://www.loc.gov/marc/community/ci00x.html

// Implement the Stringer interface for "Pretty-printing"
func (cf Controlfield) String() string {
	return fmt.Sprintf("{%s: '%s'}", cf.Tag, cf.Text)
}

// GetControlfields returns the unique set of controlfields for the
// record that match the specified tags. If no tags are specified
// (empty string) then all controlfields are returned
func (rec Record) GetControlfields(tags string) (cfs []*Controlfield) {
	if tags == "" {
		return rec.Controlfields
	}

	uniq := make(map[string]bool)
	for _, t := range strings.Split(tags, ",") {
		for _, cf := range rec.Controlfields {
			if cf.Tag == t {
				ck := strings.Join([]string{cf.Tag, cf.Text}, ":")
				_, ok := uniq[ck]
				if !ok {
					cfs = append(cfs, cf)
				}
				uniq[ck] = true
			}
		}
	}
	return cfs
}

// GetControlfield returns the text value of the first control field for the
// record that matches the specified (presumably non-repeating) tag
func (rec Record) GetControlfield(tag string) string {
	for _, cf := range rec.Controlfields {
		if cf.Tag == tag {
			return cf.Text
		}
	}
	return ""
}

// GetTag returns the tag for the controlfield
func (cf Controlfield) GetTag() string {
	return cf.Tag
}

// GetText returns the text for the controlfield
func (cf Controlfield) GetText() string {
	return cf.Text
}
