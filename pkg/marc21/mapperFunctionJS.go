package marc21

import (
	"emperror.dev/errors"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/js"
)

// MapperFunctionJS
// handles all scalar values for mapping
type MapperFunctionJS struct {
	jse *js.JSEngine
}

func NewMapperFunctionJS(jse *js.JSEngine) (*MapperFunctionJS, error) {
	return &MapperFunctionJS{jse: jse}, nil
}

func (m *MapperFunctionJS) GetExt() string {
	return "js"
}

func (m *MapperFunctionJS) Run(mapperField *Field, field *Subfield, record *Record) (*FieldTypeNoStruct, error) {
	jsInit := &js.FieldInitializer{
		Tag:      field.Datafield.Tag,
		Ind1:     field.Datafield.Ind1,
		Ind2:     field.Datafield.Ind2,
		Subfield: map[string][]string{},
	}
	for _, sf := range field.Datafield.Subfields {
		if _, ok := jsInit.Subfield[sf.GetCode()]; !ok {
			jsInit.Subfield[sf.GetCode()] = []string{}
		}
		jsInit.Subfield[sf.GetCode()] = append(jsInit.Subfield[sf.GetCode()], sf.GetText())
	}

	object, err := m.jse.RunField(mapperField.Replace, jsInit)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot run %s on field %s", mapperField.Replace, field.GetMarc())
	}

	result, err := getResult(object, mapperField.Type, mapperField.TypeFormat)
	return result, errors.WithStack(err)
}

var (
	_ MapperFunction = (*MapperFunctionJS)(nil)
)
