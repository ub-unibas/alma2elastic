package marc21

import (
	"bytes"
	"emperror.dev/errors"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"golang.org/x/exp/slices"
	"io"
	"net/http"
	"net/url"
)

const ServiceTypeMIJ = "marcinjson"
const ServiceTypeDefault = ""

type ServiceConfig struct {
	UrlStr      string
	ServiceType string
}

type CacheEntry struct {
	u *url.URL
	t string
}

type FieldInitializer struct {
	Tag      string              `json:"tag"`
	Ind1     string              `json:"ind1"`
	Ind2     string              `json:"ind2"`
	Subfield map[string][]string `json:"subfield"`
}

type QueryStruct struct {
	Name   string       `json:"name"`
	Field  *Datafield   `json:"field"`
	Object []*Datafield `json:"object"`
	Leader string       `json:"leader"`
	tagRef map[string]int
}

type QueryStructMARCIJ struct {
	Name   string       `json:"name"`
	Field  *MARCIJField `json:"field"`
	Object *MARCIJ      `json:"object"`
}

type RESTEngine struct {
	cache   map[string]CacheEntry
	client  *http.Client
	counter int64
}

func NewRESTEngine(client *http.Client) (*RESTEngine, error) {
	re := &RESTEngine{client: client}
	return re, re.New()
}

func (re *RESTEngine) New() error {
	re.cache = map[string]CacheEntry{}
	return nil
}

func (re *RESTEngine) Close() error {
	return nil
}

func (re *RESTEngine) AddCache(name string, serviceConfig ServiceConfig) error {
	u, err := url.Parse(serviceConfig.UrlStr)
	if err != nil {
		return errors.Wrapf(err, "cannot parse url '%s' of service '%s'", serviceConfig.UrlStr, name)
	}
	if !slices.Contains([]string{ServiceTypeDefault, ServiceTypeMIJ}, serviceConfig.ServiceType) {
		return errors.Errorf("invalid service type '%s'")
	}
	re.cache[name] = CacheEntry{
		u: u,
		t: serviceConfig.ServiceType,
	}
	return nil
}

func df2df(df *Datafield) *marc21struct.Datafield {
	result := &marc21struct.Datafield{
		Tag:       df.Tag,
		Ind1:      df.Ind1,
		Ind2:      df.Ind2,
		Subfields: []*marc21struct.Subfield{},
	}
	for _, sf := range df.Subfields {
		result.Subfields = append(result.Subfields, &marc21struct.Subfield{
			Code:      sf.Code,
			Text:      sf.Text,
			Datafield: nil,
		})
	}
	return result
}

func (re *RESTEngine) RunField(target string, replace string, init *Datafield, rec *Record) (any, error) {
	var err error
	s, ok := re.cache[replace]
	if !ok {
		return nil, errors.Errorf("service '%s' for source field '%s%s%s' not in cache", init.Tag, init.Ind1, init.Ind2)
	}
	var queryData []byte
	switch s.t {
	case ServiceTypeDefault:
		queryStruct := &marc21struct.QueryStruct{
			Name:       target,
			Field:      df2df(init),
			Datafields: []*marc21struct.Datafield{},
			Leader:     rec.Leader.GetText(),
		}
		for _, df := range rec.Datafields {
			queryStruct.Datafields = append(queryStruct.Datafields, df2df(df))
		}
		queryData, err = json.Marshal(queryStruct)
		if err != nil {
			return nil, errors.Wrapf(err, "cannot marshal %v", queryStruct)
		}
	case ServiceTypeMIJ:
		pmr := &MARCIJ{
			Leader: "",
			Fields: []*MARCIJField{},
		}
		if err := pmr.fromMarc(rec); err != nil {
			return nil, errors.Wrap(err, "cannot convert record")
		}
		fld := &MARCIJField{}
		if err := fld.fromMarc(init); err != nil {
			return nil, errors.WithStack(err)
		}
		queryStruct := &QueryStructMARCIJ{
			Name:   target,
			Field:  fld,
			Object: pmr,
		}
		queryData, err = json.Marshal(queryStruct)
		if err != nil {
			return nil, errors.Wrapf(err, "cannot marshal %v", pmr)
		}
	default:
		return nil, errors.Errorf("cannot run '%s' - invalid service type '%s'", replace, s.t)
	}

	re.counter++
	resp, err := re.client.Post(s.u.String(), "application/json", bytes.NewBuffer(queryData))
	if err != nil {
		return nil, errors.Wrapf(err, "cannot query '%s'", s.u.String())
	}
	resultData, err := io.ReadAll(resp.Body)
	err = resp.Body.Close()
	if err != nil {
		log.Error().Err(err).Msgf("Failed to close response body %v.", err)
		return nil, err
	}
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get result body of '%s'", s.u.String())
	}
	if resp.StatusCode >= 300 {
		return nil, errors.Errorf("query '%s' - status %v-%s: %s: %s", s.u.String(), resp.StatusCode, resp.Status, string(resultData))
	}
	if resp.StatusCode >= 400 {
		return nil, errors.Errorf("Created a bad request: %s.", string(resultData))
	}
	if resp.StatusCode >= 500 {
		return nil, errors.Errorf("Server had an internal server error: %s.", string(resultData))
	}
	var result []any
	if err = json.Unmarshal(resultData, &result); err != nil {
		var result2 any
		if err = json.Unmarshal(resultData, &result2); err != nil {
			return nil, errors.Errorf("cannot unmarshal result data '%s'", string(resultData))
		}
		result = []any{result2}
	}
	return any(result), nil
}
