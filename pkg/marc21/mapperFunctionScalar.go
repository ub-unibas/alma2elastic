package marc21

import (
	"emperror.dev/errors"
)

// MapperFunctionScalar
// handles all scalar values for mapping
type MapperFunctionScalar struct {
}

func NewMapperFunctionScalar() (*MapperFunctionScalar, error) {
	return &MapperFunctionScalar{}, nil
}

func (m *MapperFunctionScalar) GetExt() string {
	return ""
}
func (m *MapperFunctionScalar) Run(mapperField *Field, field *Subfield, record *Record) (*FieldTypeNoStruct, error) {
	var valstr = field.GetText()
	/*
		if mapperField.Regexp != nil {
			if !mapperField.Regexp.MatchString(valstr) {
				return nil, nil
			}
		}
	*/
	if mapperField.Regexp != nil {
		valstr = mapperField.Regexp.ReplaceAllString(valstr, mapperField.Replace)
	}
	result, err := getResult(valstr, mapperField.Type, mapperField.TypeFormat)
	return result, errors.WithStack(err)
}

var (
	_ MapperFunction = (*MapperFunctionScalar)(nil)
)
