package marc21

import (
	"emperror.dev/errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type MapperFunction interface {
	GetExt() string
	Run(mapperField *Field, field *Subfield, record *Record) (*FieldTypeNoStruct, error)
}

type MapperManager struct {
	funcs map[string]MapperFunction
}

func NewMapperManager() (*MapperManager, error) {
	return &MapperManager{funcs: map[string]MapperFunction{}}, nil
}

func (m *MapperManager) GetExt() string {
	return ""
}

func (m *MapperManager) Add(mapperFunction MapperFunction) {
	m.funcs[mapperFunction.GetExt()] = mapperFunction
}

func (m *MapperManager) Run(mapperField *Field, field *Subfield, record *Record) (*FieldTypeNoStruct, error) {
	pos := strings.LastIndex(mapperField.Replace, ".")
	var ext string
	if pos > 0 {
		ext = mapperField.Replace[pos+1:]
	}
	mapperFunction, ok := m.funcs[ext]
	if !ok {
		mapperFunction, ok = m.funcs[""]
		if !ok {
			return nil, errors.New("no default mapper available")
		}
	}

	result, err := mapperFunction.Run(mapperField, field, record)
	return result, errors.WithStack(err)
}

func getResult(val any, typ, typeFormat string) (*FieldTypeNoStruct, error) {
	result := &FieldTypeNoStruct{
		Keyword:   []string{},
		Date:      []Date{},
		DateTime:  []DateTime{},
		Long:      []int64{},
		Double:    []float64{},
		Object:    []any{},
		GeoShape:  []*GeoShape{},
		Agent:     []*Agent{},
		Concept:   []*Concept{},
		Hierarchy: []Hierarchy{},
		DateRange: []DateRange{},
	}
	valstr, ok := val.(string)
	if !ok && typ != "object" {
		valstr = fmt.Sprintf("%v", val)
	}
	switch typ {
	case "date":
		if typeFormat != "" {
			t, err := time.Parse(typeFormat, valstr)
			if err != nil {
				return nil, errors.Wrapf(err, "cannot parse date %s with format %s", valstr, typeFormat)
			}
			result.Date = append(result.Date, Date(t))
		}
	case "datetime":
		if typeFormat != "" {
			t, err := time.Parse(typeFormat, valstr)
			if err != nil {
				return nil, errors.Wrapf(err, "cannot parse date %s with format %s", valstr, typeFormat)
			}
			result.DateTime = append(result.DateTime, DateTime(t))
		}
	case "long":
		long, err := strconv.ParseInt(valstr, 10, 64)
		if err != nil {
			return nil, errors.Wrapf(err, "cannot parse int %s", valstr)
		}
		result.Long = append(result.Long, long)
	case "double":
		double, err := strconv.ParseFloat(valstr, 64)
		if err != nil {
			return nil, errors.Wrapf(err, "cannot parse double %s", valstr)
		}
		result.Double = append(result.Double, double)
	case "agent": // agent is a special case
		if m, ok := val.(map[string]any); ok {
			agent := &Agent{
				Concept: Concept{
					Identifier: []string{},
				},
				Role: []string{},
			}
			if nameAny, ok := m["name"]; ok {
				if name, ok := nameAny.(string); ok {
					agent.Label = name
					if identifierAny, ok := m["identifier"]; ok {
						if identifierSlice, ok := identifierAny.([]any); ok {
							for _, identifierAny := range identifierSlice {
								if identifier, ok := identifierAny.(string); ok {
									agent.Identifier = append(agent.Identifier, identifier)
								}
							}
						}
					}
					if roleAny, ok := m["role"]; ok {
						if roleSlice, ok := roleAny.([]any); ok {
							for _, roleAny := range roleSlice {
								if role, ok := roleAny.(string); ok {
									agent.Role = append(agent.Role, role)
								}
							}
						}
					}
				}
				result.Agent = append(result.Agent, agent)
			}
		}
	case "concept": // agent is a special case
		if m, ok := val.(map[string]any); ok {
			concept := &Concept{
				Identifier: []string{},
			}
			if nameAny, ok := m["label"]; ok {
				if name, ok := nameAny.(string); ok {
					concept.Label = name
					if identifierAny, ok := m["identifier"]; ok {
						if identifierSlice, ok := identifierAny.([]any); ok {
							for _, identifierAny := range identifierSlice {
								if identifier, ok := identifierAny.(string); ok {
									concept.Identifier = append(concept.Identifier, identifier)
								}
							}
						}
					}
				}
				result.Concept = append(result.Concept, concept)
			}
		}
	case "object":
		if list, ok := val.([]any); ok {
			for _, v := range list {
				result.Object = append(result.Object, v)
			}
		} else {
			result.Object = append(result.Object, val)
		}
	case "hierarchy":
		result.Hierarchy = append(result.Hierarchy, Hierarchy(valstr))
	default:
		result.Keyword = append(result.Keyword, valstr)
	}
	return result, nil
}

var (
	_ MapperFunction = (*MapperManager)(nil)
)
