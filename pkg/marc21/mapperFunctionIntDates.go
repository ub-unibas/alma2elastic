package marc21

import (
	"regexp"
	"strconv"
	"strings"
	"time"
)

type DateResult struct {
	Gte string `json:"gte,omitempty"`
	Lte string `json:"lte,omitempty"`
}

// Check if the date string is valid
func isValidDate(date string) bool {
	var layouts = [3]string{"2006", "200601", "20060102"}
	for _, layout := range layouts {
		_, err := time.Parse(layout, date)
		if err == nil {
			return true
		}
	}
	return false
}

func date(target string, replace string, init *Datafield, rec *Record) ([]any, error) {
	var objects = map[string][]*Datafield{}
	for _, object := range rec.Datafields {
		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}
	var controlfields = map[string][]*Controlfield{}
	for _, cf := range rec.Controlfields {
		if _, ok := controlfields[cf.Tag]; !ok {
			controlfields[cf.Tag] = []*Controlfield{}
		}
		controlfields[cf.Tag] = append(controlfields[cf.Tag], cf)
	}

	var result = DateResult{}
	var field046, ok = objects["046"]
	if ok {
		for _, field := range field046 {
			var dateFirst string
			var dateSecond string
			var datetype string
			var gte string
			var lte string
			for _, subField := range field.Subfields {
				if subField.Code == "a" {
					datetype = subField.Text
				}
				if subField.Code == "c" {
					matched, _ := regexp.MatchString(`^[0-9.\[\]]*$`, subField.Text)
					if matched == true {
						dateFirst = strings.Replace(strings.Replace(strings.Replace(subField.Text, ".", "", -1), "[", "", -1), "]", "", -1)
					}
				}
				if subField.Code == "e" {
					matched, _ := regexp.MatchString(`^[0-9.\[\]]*$`, subField.Text)
					if matched == true {
						dateSecond = strings.Replace(strings.Replace(strings.Replace(subField.Text, ".", "", -1), "[", "", -1), "]", "", -1)
					}
				}
			}
			switch datetype {
			case "e":
				gte = dateFirst + dateSecond
				lte = dateFirst + dateSecond
			case "s", "p", "t":
				gte = dateFirst
				lte = dateFirst
			case "m", "r":
				gte = dateFirst
				if dateSecond != "9999" {
					lte = dateSecond
				} else {
					lte = dateFirst
				}
			case "c", "d", "i", "k", "u", "q":
				gte = dateFirst
				lte = strings.Replace(dateSecond, "9999", strconv.Itoa(time.Now().Year()), -1)
			}
			if gte != "0000" && isValidDate(gte) {
				result.Gte = gte
				if lte != "9999" && isValidDate(lte) {
					result.Lte = lte
				}
			}
		}
	}

	if result.Gte == "" && result.Lte == "" {
		/* if no field 046 or no result from 046, take 008/07-14 */
		var cf008, ok = controlfields["008"]
		if !ok {
			return nil, nil
		}
		for _, field := range cf008 {
			var dateFirst = strings.Replace(strings.Replace(field.Text[7:11], "u", "0", -1), " ", "0", -1)
			var dateSecond = strings.Replace(strings.Replace(field.Text[11:15], "u", "9", -1), " ", "9", -1)
			var gte string
			var lte string
			switch field.Text[6:7] {
			case "e":
				gte = dateFirst + strings.Replace(dateSecond, "99", "", -1)
				lte = dateFirst + strings.Replace(dateSecond, "99", "", -1)
			case "s", "p", "t":
				gte = dateFirst
				lte = dateFirst
			case "m", "r":
				gte = dateFirst
				if dateSecond != "9999" {
					lte = dateSecond
				} else {
					lte = dateFirst
				}
			case "c", "d", "i", "k", "u", "q":
				gte = dateFirst
				lte = strings.Replace(dateSecond, "9999", strconv.Itoa(time.Now().Year()), -1)
			}
			if gte != "0000" && isValidDate(gte) {
				result.Gte = gte
				if lte != "9999" && isValidDate(lte) {
					result.Lte = lte
				}
			}
		}
	}

	if target == "date" {
		if result.Gte == "" || result.Lte == "" {
			return []any{result}, nil
		} else {
			return []any{result}, nil
		}
	} else if target == "daterange" {
		if result.Gte == "" || result.Lte == "" || result.Lte < result.Gte {
			return []any{}, nil
		} else {
			return []any{result}, nil
		}
	} else {
		return []any{}, nil
	}
}
