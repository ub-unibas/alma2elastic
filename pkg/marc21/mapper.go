package marc21

import (
	"emperror.dev/errors"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/js"
	"golang.org/x/exp/slices"
	"reflect"
	"regexp"
)

type Field struct {
	Flags             []string
	MarcField         *regexp.Regexp
	Target            string
	Regexp            *regexp.Regexp
	Replace           string
	QualifierSubfield string
	QRegexp           *regexp.Regexp
	Type              string
	TypeFormat        string
}

type MapperFields []*Field
type Mapper struct {
	MapperFields
	jse           *js.JSEngine
	re            *RESTEngine
	mapperManager *MapperManager
}

func appendObjectIfNew(objects []any, object ...any) []any {
	for _, o := range object {
		if !slices.ContainsFunc(objects, func(a any) bool {
			return reflect.DeepEqual(a, o)
		}) {
			objects = append(objects, o)
		}
	}
	return objects
}

func NewMapper(mapperManager *MapperManager, jse *js.JSEngine, re *RESTEngine) (*Mapper, error) {
	mapper := &Mapper{
		MapperFields:  []*Field{},
		jse:           jse,
		re:            re,
		mapperManager: mapperManager,
	}
	return mapper, nil
}

func (m *Mapper) Map(field *Subfield, flags []string, record *Record) (map[string]*FieldTypeNoStruct, error) {
	marc := field.GetMarc()
	result := map[string]*FieldTypeNoStruct{}
	for _, mm := range m.MapperFields {
		// ignore if flags are not compatible
		if !contains(flags, mm.Flags) {
			continue
		}
		// ignore if field not matching
		if !mm.MarcField.MatchString(marc) {
			continue
		}
		if mm.QualifierSubfield != "" {
			qOK := false
			for _, qSub := range field.Datafield.Subfields {
				if qSub.GetCode() == mm.QualifierSubfield {
					if mm.QRegexp.MatchString(qSub.GetText()) {
						qOK = true
						break
					}
				}
			}
			if !qOK {
				continue
			}
		}

		if mm.Regexp != nil {
			var valstr = field.GetText()
			if !mm.Regexp.MatchString(valstr) {
				continue
			}
		}

		resultElement, err := m.mapperManager.Run(mm, field, record)
		if err != nil {
			return nil, errors.Wrapf(err, "cannot map %s", mm.Replace)
		}
		if resultElement != nil {
			if _, ok := result[mm.Target]; ok == false {
				result[mm.Target] = &FieldTypeNoStruct{
					Keyword:   []string{},
					Date:      []Date{},
					DateTime:  []DateTime{},
					Long:      []int64{},
					Double:    []float64{},
					Object:    []any{},
					GeoShape:  []*GeoShape{},
					Agent:     []*Agent{},
					Concept:   []*Concept{},
					Hierarchy: []Hierarchy{},
					DateRange: []DateRange{},
				}
			}
			result[mm.Target].Keyword = append(result[mm.Target].Keyword, resultElement.Keyword...)
			result[mm.Target].Date = append(result[mm.Target].Date, resultElement.Date...)
			result[mm.Target].DateTime = append(result[mm.Target].DateTime, resultElement.DateTime...)
			result[mm.Target].Long = append(result[mm.Target].Long, resultElement.Long...)
			result[mm.Target].Double = append(result[mm.Target].Double, resultElement.Double...)
			result[mm.Target].Object = append(result[mm.Target].Object, resultElement.Object...)
			result[mm.Target].Agent = append(result[mm.Target].Agent, resultElement.Agent...)
			result[mm.Target].Concept = append(result[mm.Target].Concept, resultElement.Concept...)
			result[mm.Target].Hierarchy = append(result[mm.Target].Hierarchy, resultElement.Hierarchy...)
			result[mm.Target].DateRange = append(result[mm.Target].DateRange, resultElement.DateRange...)
		}
	}
	return result, nil
}

func (m *Mapper) MapFacet(field *Subfield, flags []string, rec *Record) ([]*Facet, error) {
	facetMap, err := m.Map(field, flags, rec)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot map field \"%s\"", field.GetMarc())
	}
	result := []*Facet{}
	for k, f := range facetMap {

		result = append(result, &Facet{
			Name:      k,
			FieldType: (*FieldType)(f),
		})
	}
	return result, nil
}

func (m *Mapper) MapACL(field *Subfield, flags []string) (map[string][]string, error) {
	aclMap, err := m.Map(field, flags, nil)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot map field \"%s\"", field.GetMarc())
	}
	result := map[string][]string{}
	for k, f := range aclMap {
		if _, ok := result[k]; ok {
			result[k] = []string{}
		}
		result[k] = append(result[k], f.Keyword...)
	}
	return result, nil
}

func (m *Mapper) MapFlags(field *Subfield) ([]string, error) {
	marc := field.GetMarc()
	valstr := field.GetText()
	result := []string{}
	for _, mm := range m.MapperFields {
		// ignore if field not matching
		if !mm.MarcField.MatchString(marc) {
			continue
		}
		if mm.QualifierSubfield != "" {
			qOK := false
			for _, qSub := range field.Datafield.Subfields {
				if qSub.GetCode() == mm.QualifierSubfield {
					if mm.QRegexp.MatchString(qSub.GetText()) {
						qOK = true
						break
					}
				}
			}
			if !qOK {
				continue
			}
		}

		if mm.Regexp != nil {
			if mm.Regexp.MatchString(valstr) {
				for _, flag := range mm.Flags {
					if !slices.Contains(result, flag) {
						result = append(result, flag)
					}
				}
			}
		}
	}
	return result, nil
}
