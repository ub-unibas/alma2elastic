package marc21

import (
	"fmt"
	"golang.org/x/exp/slices"
)

type DDCResult []string

func ddc(target string, replace string, init *Datafield, rec *Record) ([]any, error) {
	var objects = map[string][]*Datafield{}
	for _, object := range rec.Datafields {
		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var result = DDCResult{}
	var field082, ok = objects["082"]
	if !ok {
		return nil, nil
	}
	for _, field := range field082 {
		var ddc string
		for _, subField := range field.Subfields {
			if subField.Code == "a" {
				ddc = subField.Text
			}
		}
		if ddc == "" {
			return nil, nil
		}
		var content string
		var counter = 1
		for i := 0; i < len(ddc); i++ {
			if slices.Contains([]rune{'.', '/'}, rune(ddc[i])) {
				continue
			}
			if !slices.Contains([]rune{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}, rune(ddc[i])) {
				break
			}
			if content != "" {
				content += "!!"
			}
			content += ddc[0 : i+1]
			result = append(result, fmt.Sprintf("ddc:%03d:%s", counter, content))
			counter++
		}
	}
	//	slices.Sort(result)
	result = slices.Compact(result)
	var ret = make([]any, len(result))
	for i, v := range result {
		ret[i] = v
	}
	return ret, nil
}
