package marc21

type Holding struct {
	Library     string `json:"library"`
	Location    string `json:"location,omitempty"`
	CallNumber  string `json:"callNumber,omitempty"`
	Note        string `json:"note,omitempty"`
	Summary     string `json:"summary,omitempty"`
	SummaryNote string `json:"summaryNote,omitempty"`
	Items       []Item `json:"items,omitempty"`
}

type Item struct {
	ItemCallNumber string `json:"itemCallNumber,omitempty"`
	Note           string `json:"note,omitempty"`
}

func (i Item) isEmpty() bool {
	return i.ItemCallNumber == "" && i.Note == ""
}

type HoldingResult []Holding

func holding(target string, replace string, init *Datafield, rec *Record) ([]any, error) {
	var objects = map[string][]*Datafield{}
	for _, object := range rec.Datafields {
		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = HoldingResult{}
	var field852, ok = objects["852"]
	if !ok {
		field852 = nil
	}
	field866, ok := objects["866"]
	if !ok {
		field866 = nil
	}
	field949, ok := objects["949"]
	if !ok {
		field949 = nil
	}
	for _, field := range field852 {
		//		var result = HoldingResult{}
		var linkedField string
		var library string
		var location string
		var callNumber string
		var note string
		var summary string
		var summaryNote string
		var items []Item
		for _, subField := range field.Subfields {
			var ok bool
			if field.Tag == "852" {
				ok = true
			}
			if ok {
				switch subField.Code {
				case "b":
					library = subField.Text
				case "c":
					location = subField.Text
				case "j":
					callNumber = subField.Text
				case "z":
					if note != "" {
						note += ", " + subField.Text
					} else {
						note = subField.Text
					}
				case "8":
					linkedField = subField.Text
				}
			}
		}
		if linkedField != "" {
			for _, field := range field866 {
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "8" {
						if subField.Text == linkedField {
							found = true
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						summary = subField.Text
					case "z":
						if summaryNote != "" {
							summaryNote += ", " + subField.Text
						} else {
							summaryNote = subField.Text
						}
					}
				}
			}

			// Revised loop for the field949
			for _, field := range field949 {
				var found bool
				var itemResult Item // Redefine itemResult in this scope to avoid added multiple times
				for _, subField := range field.Subfields {
					if subField.Code == "3" && subField.Text == linkedField {
						found = true
						break
					}
				}
				if found {
					for _, subField := range field.Subfields {
						switch subField.Code {
						case "h":
							itemResult.ItemCallNumber = subField.Text
						case "q":
							if itemResult.Note != "" {
								itemResult.Note += ", " + subField.Text
							} else {
								itemResult.Note = subField.Text
							}
						case "z":
							if itemResult.Note != "" {
								itemResult.Note += ", " + subField.Text
							} else {
								itemResult.Note = subField.Text
							}
						}
					}
					if !itemResult.isEmpty() {
						items = append(items, itemResult)
					}
				}
			}
		}
		if library != "" {
			allResult = append(allResult, Holding{
				Library:     library,
				Location:    location,
				CallNumber:  callNumber,
				Note:        note,
				Summary:     summary,
				SummaryNote: summaryNote,
				Items:       items,
			})
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
