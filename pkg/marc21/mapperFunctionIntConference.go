package marc21

import "strings"

type Conference struct {
	Name        string   `json:"namePart"`
	Date        string   `json:"date,omitempty"`
	Description []string `json:"description,omitempty"`
	Role        []string `json:"role,omitempty"`
	Identifier  string   `json:"identifier,omitempty"`
}

type ConferenceResult map[string][]Conference

func conference(target string, replace string, init *Datafield, rec *Record) ([]any, error) {
	var objects = map[string][]*Datafield{}
	for _, object := range rec.Datafields {
		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []ConferenceResult{}
	var personFields = []*Datafield{}
	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}
	field711, ok := objects["711"]
	if ok {
		personFields = append(personFields, field711...)
	}
	field111, ok := objects["111"]
	if ok {
		personFields = append(personFields, field111...)
	}
	for _, field := range personFields {
		var result = ConferenceResult{}
		var linkedField string
		var name string
		var date string
		var description []string
		var role []string
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			var ok bool
			if field.Tag == "111" {
				ok = true
			}
			if field.Tag == "711" && field.Ind2 != "2" {
				ok = true
			}
			if ok {
				switch subField.Code {
				case "a":
					name = subField.Text
				case "n":
					name += " " + subField.Text
				case "c":
					name += ", " + subField.Text
				case "e":
					name += " " + subField.Text
				case "d":
					date = subField.Text
				case "g":
					description = append(description, subField.Text)
				case "0":
					identifier = subField.Text
				case "4":
					role = append(role, subField.Text)
				case "6":
					linkedField = subField.Text
				}
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		if name != "" {
			if _, ok := result[authority]; !ok {
				result[authority] = []Conference{}
			}
			result[authority] = append(result[authority], Conference{
				Name:        name,
				Date:        date,
				Description: description,
				Role:        role,
				Identifier:  identifier,
			})
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var date string
				var description []string
				var role []string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "111-") || strings.HasPrefix(subField.Text, "711-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						name = subField.Text
					case "n":
						name += " " + subField.Text
					case "c":
						name += ", " + subField.Text
					case "e":
						name += " " + subField.Text
					case "d":
						date = subField.Text
					case "g":
						description = append(description, subField.Text)
					case "0":
						identifier = subField.Text
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if name != "" {
					if _, ok := result[authority]; !ok {
						result[authority] = []Conference{}
					}
					result[authority] = append(result[authority], Conference{
						Name:        name,
						Date:        date,
						Description: description,
						Role:        role,
						Identifier:  identifier,
					})
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
