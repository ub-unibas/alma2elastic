package js

import (
	"emperror.dev/errors"
	"encoding/json"
	"fmt"
	"github.com/je4/utils/v2/pkg/zLogger"
	v8 "rogchap.com/v8go"
	"time"
)

type Isolate struct {
	iso              *v8.Isolate
	callCounter      uint64
	scriptCache      map[string]*v8.UnboundScript
	scripts          map[string]string
	logger           zLogger.ZLogger
	duration         time.Duration
	isolationRefresh uint64
}

func NewIsolate(scripts map[string]string, isolationRefresh uint64, logger zLogger.ZLogger) (*Isolate, error) {
	iso := &Isolate{
		iso:              nil,
		callCounter:      0,
		scriptCache:      map[string]*v8.UnboundScript{},
		scripts:          scripts,
		logger:           logger,
		isolationRefresh: isolationRefresh,
	}
	return iso, errors.WithStack(iso.Init())
}
func (iso *Isolate) Init() error {
	iso.logger.Info().Msg("initializing isolate")
	iso.callCounter = 0
	if iso.iso != nil {
		iso.iso.Dispose()
	}
	iso.iso = v8.NewIsolate()
	return errors.WithStack(iso.BuildCache())
}
func (iso *Isolate) Close() {
	if iso.iso != nil {
		iso.iso.Dispose()
	}
}

func (iso *Isolate) BuildCache() (err error) {
	iso.duration = 0
	iso.scriptCache = map[string]*v8.UnboundScript{}
	for name, source := range iso.scripts {
		iso.scriptCache[name], err = iso.iso.CompileUnboundScript(source, name, v8.CompileOptions{})
		if err != nil {
			return errors.Wrapf(err, "cannot compile script '%s'", name)
		}
	}
	return nil
}

func (iso *Isolate) RunField(name string, init *FieldInitializer) (any, error) {
	iso.callCounter++
	if iso.callCounter%iso.isolationRefresh == 0 {
		iso.Init()
	}
	jsFunc, ok := iso.scriptCache[name]
	if !ok {
		return nil, errors.Errorf("function '%s' not found", name)
	}
	global := v8.NewObjectTemplate(iso.iso)
	objField := v8.NewObjectTemplate(iso.iso)
	objField.Set("Tag", init.Tag)
	objField.Set("Ind1", init.Ind1)
	objField.Set("Ind2", init.Ind2)
	objSubField := v8.NewObjectTemplate(iso.iso)
	for key, vals := range init.Subfield {
		list := v8.NewObjectTemplate(iso.iso)
		for idx, val := range vals {
			list.Set(fmt.Sprintf("%d", idx), val)
		}
		objSubField.Set(key, list)
	}
	objField.Set("Subfield", objSubField)
	global.Set("init", objField)

	ctx := v8.NewContext(iso.iso, global)
	defer ctx.Close()
	//	jse.logger.Debugf("starting '%s'", name)
	//	startTime := time.Now()
	val, err := jsFunc.Run(ctx)
	if err != nil {
		return nil, errors.Wrapf(err, "error executing %s", name)
	}
	/*
		duration := time.Now().Sub(startTime)
		if duration > iso.duration {
			iso.duration = duration
			iso.logger.Debugf("slow function '%s' - %s // call %d", name, duration.String(), iso.callCounter)
		}
	*/
	var result any
	if val.IsObject() {
		valJSON, err := val.Object().MarshalJSON()
		if err != nil {
			return nil, errors.Wrapf(err, "cannot marshal result of %s - %s", name, val.String())
		}
		result = map[string]any{}
		if valJSON[0] != '{' {
			return nil, errors.Errorf("invalid result %s from %s. need structure", string(valJSON), name)
		}
		if err := json.Unmarshal(valJSON, &result); err != nil {
			return nil, errors.Wrapf(err, "cannot unmarshal result of %s - %s", name, string(valJSON))
		}
	} else {
		valJSON, err := val.MarshalJSON()
		if err != nil {
			return nil, errors.Wrapf(err, "cannot marshal result of %s - %s", name, val.String())
		}
		result = map[string]any{}
		if valJSON[0] != '{' {
			return nil, errors.Errorf("invalid result %s from %s. need structure", string(valJSON), name)
		}
		if err := json.Unmarshal(valJSON, &result); err != nil {
			return nil, errors.Wrapf(err, "cannot unmarshal result of %s - %s", name, string(valJSON))
		}
	}
	return result, nil
}
