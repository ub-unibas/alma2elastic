package js

import (
	"emperror.dev/errors"
	"fmt"
	"github.com/je4/utils/v2/pkg/zLogger"
	v8 "rogchap.com/v8go"
	"sync"
	"time"
)

type JSEngine struct {
	iso []*Isolate
	//cache    []map[string]*v8.UnboundScript
	scripts          map[string]string
	numVM            int
	vmNums           chan int
	freeLock         sync.Mutex
	duration         time.Duration
	logger           zLogger.ZLogger
	isolationRefresh uint64
}

type FieldInitializer struct {
	Tag      string              `json:"tag"`
	Ind1     string              `json:"ind1"`
	Ind2     string              `json:"ind2"`
	Subfield map[string][]string `json:"subfield"`
}

func NewJSEngine(numVM int, isolationRefresh uint64, logger zLogger.ZLogger) (*JSEngine, error) {
	engine := &JSEngine{
		numVM:            numVM,
		vmNums:           make(chan int, numVM),
		logger:           logger,
		scripts:          map[string]string{},
		isolationRefresh: isolationRefresh,
	}
	return engine, nil
}

func (jse *JSEngine) Startup() error {
	//	jse.cache = []map[string]*v8.UnboundScript{}
	jse.iso = []*Isolate{}
	for i := 0; i < jse.numVM; i++ {
		iso, err := NewIsolate(jse.scripts, jse.isolationRefresh, jse.logger)
		if err != nil {
			return errors.Wrap(err, "cannot create new isolate")
		}
		jse.iso = append(jse.iso, iso)
		jse.vmNums <- i
	}
	return nil
}

func (jse *JSEngine) Close() {
	for _, iso := range jse.iso {
		iso.Close()
	}
}

func (jse *JSEngine) AddScript(name, source string) error {
	jse.scripts[name] = source
	return nil
}

func (jse *JSEngine) getVM() (int, error) {
	select {
	case num := <-jse.vmNums:
		return num, nil
	case <-time.After(3 * time.Second):
		return 0, errors.Errorf("timout for getting javascript VM")
	}
}

func (jse *JSEngine) releaseVM(vmNo int) {
	jse.vmNums <- vmNo
}

func (jse *JSEngine) RunField(name string, init *FieldInitializer) (any, error) {
	vmNo, err := jse.getVM()
	if err != nil {
		return nil, errors.WithStack(err)
	}
	defer jse.releaseVM(vmNo)

	iso := jse.iso[vmNo]
	val, err := iso.RunField(name, init)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return val, nil
}

func x() {
	ctx := v8.NewContext()                                  // creates a new V8 context with a new Isolate aka VM
	ctx.RunScript("const add = (a, b) => a + b", "math.js") // executes a script on the global context
	ctx.RunScript("const result = add(3, 4)", "main.js")    // any functions previously added to the context can be called
	val, _ := ctx.RunScript("result", "value.js")           // return a value in JavaScript back to Go
	fmt.Printf("addition result: %s", val)
}
